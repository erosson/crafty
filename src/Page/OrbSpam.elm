module Page.OrbSpam exposing (Model, Msg, init, update, view)

import Browser.Dom
import Crafting.Item exposing (Item)
import Crafting.Orb exposing (Orb, SpamProgress)
import Datamine.BaseItem as BaseItem exposing (BaseItem)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Json.Encode as JE
import List.Extra
import Maybe.Extra
import Plan exposing (Plan)
import Process
import Random exposing (Generator)
import Route exposing (Route)
import Session exposing (Session)
import Task
import View.Criteria
import View.Item
import View.ItemState exposing (ItemState)
import View.Nav
import View.Orb
import View.Util exposing (formatFloat, nbsp, percent)


type alias Model =
    { session : Session
    , criteriaSearch : Maybe String
    , baseItemSearch : Maybe String
    , orbSpam : Maybe SpamProgress
    , plan : Result String Plan
    }


init : Session -> Maybe String -> ( Model, Cmd Msg )
init session planStr =
    let
        model =
            Model session Nothing Nothing Nothing <| Plan.mdecode session.datamine planStr
    in
    run model |> Result.withDefault ( model, Cmd.none )


simRoute : Model -> Route
simRoute model =
    case model.plan of
        Err _ ->
            Route.Sim Nothing

        Ok plan ->
            plan.item.val
                |> Crafting.Item.encoder
                |> JE.encode 0
                |> Just
                |> Route.Sim


view : Model -> List (Html Msg)
view model =
    [ div [ class "container" ]
        [ View.Nav.view { sim = Just <| simRoute model, orbspam = Just <| Route.OrbSpam <| Maybe.map Plan.encode <| Result.toMaybe model.plan }
        , div [] <|
            case model.plan of
                Err err ->
                    [ code [] [ text err ] ]

                Ok plan ->
                    [ H.form [ class "home2", onSubmit Run ] <|
                        [ div [ class "form-group row" ]
                            [ label [ class "col-sm-2" ] [ div [] [ text "Base item" ], div [] [ a [ Route.href <| simRoute model ] [ text "To Simulator >>" ] ] ]
                            , div [ class "col-sm-10 item" ]
                                [ View.Item.viewHeaderSelector model.session.datamine plan.item.val (Maybe.withDefault "" model.baseItemSearch)
                                    |> List.map (H.map BaseMsg)
                                    |> div []
                                , hr [] []
                                , div [] <| View.Item.viewSimpleMods model.session.datamine plan.item.val
                                ]
                            ]
                        , div [ class "form-group row" ]
                            [ label [ class "col-sm-2" ] [ text "Crafting steps" ]
                            , div [ class "col-sm-10" ]
                                [ ol [ class "pl-0" ]
                                    ((plan.recipe |> List.indexedMap (\i orb -> li [] (viewRecipeDropdown i (Just orb))))
                                        ++ [ li [] (viewRecipeDropdown (List.length plan.recipe) Nothing) ]
                                    )
                                ]
                            ]
                        , div [ class "form-group row" ]
                            [ label [ class "col-sm-2" ] [ text "Iterations" ]
                            , div [ class "col-sm-10 form-inline" ]
                                [ input
                                    [ class "form-control"
                                    , onInput InputIterations
                                    , value <| String.fromInt plan.iterations
                                    ]
                                    []
                                , text <| nbsp ++ "crafting attempts"
                                ]
                            ]
                        , div [ class "form-group row" ]
                            [ label [ class "col-sm-2" ] [ text "Success criteria" ]
                            , div [ class "col-sm-10" ]
                                (View.Criteria.view model.session.datamine (toCriteriaModel plan model) plan.item.possibleModsScoured
                                    |> List.map (H.map CriteriaMsg)
                                )
                            ]
                        , div [ class "row" ]
                            (case model.orbSpam of
                                Nothing ->
                                    [ div [ class "label col-sm-2" ] [ viewRunButton plan model ] ]

                                Just progress ->
                                    viewOrbSpam plan model progress
                            )
                        ]
                    ]
        ]
    ]


viewRunButton : Plan -> Model -> Html Msg
viewRunButton plan model =
    button
        [ class "btn btn-primary"
        , type_ "submit"
        , disabled (not <| isFormSubmittable plan model)
        ]
        [ text "Run" ]


isFormSubmittable : Plan -> Model -> Bool
isFormSubmittable plan model =
    not (plan.recipe == [] || plan.criteria == [] || Maybe.Extra.unwrap False (Crafting.Orb.isSpamDone >> not) model.orbSpam)


viewOrbSpam : Plan -> Model -> SpamProgress -> List (Html Msg)
viewOrbSpam plan model p =
    [ div [ class "label col-sm-2" ]
        [ if Crafting.Orb.isSpamDone p then
            viewRunButton plan model

          else
            button
                [ type_ "button"
                , class "btn btn-dark-outline"
                , onClick RunCancel
                ]
                [ text "Cancel" ]
        ]
    , div [ class "col-sm-10" ]
        [ -- , progress [ value <| String.fromInt p.generated, A.max <| String.fromInt p.target ] []
          div [ class "progress" ]
            [ div [ class "progress-bar", style "width" <| percent <| Crafting.Orb.spamPercentDone p ] []
            ]
        , div []
            [ text <|
                "Finished crafts: "
                    ++ String.fromInt p.generated
                    ++ "/"
                    ++ String.fromInt p.target
                    ++ " ("
                    ++ percent (Crafting.Orb.spamPercentDone p)
                    ++ ")"
            ]
        , div []
            [ text <|
                "Successful crafts: "
                    ++ String.fromInt p.passed
                    ++ " ("
                    ++ percent (Crafting.Orb.spamPercentPassed p)
                    ++ ")"
            ]
        , div []
            (if p.passed == 0 then
                [ text "This craft ", b [] [ text "never" ], text " succeeds!" ]

             else if p.passed == p.generated then
                [ text "This craft ", b [] [ text "always" ], text " succeeds!" ]

             else
                [ text "One successful craft every "
                , b [] [ text <| formatFloat 4 <| 1 / Crafting.Orb.spamPercentPassed p ]
                , text " attempts"
                ]
            )
        ]
    ]


viewRecipeDropdown : Int -> Maybe Orb -> List (Html Msg)
viewRecipeDropdown index selected =
    [ span [ class "dropdown" ]
        [ button
            -- type=button avoids form submit
            [ type_ "button"
            , class "btn btn-outline-dark dropdown-toggle"
            , A.attribute "data-toggle" "dropdown"
            , A.attribute "data-dropup-auto" "false"
            ]
            (case selected |> Maybe.map View.Orb.orbView of
                Nothing ->
                    [ text "+ Add Crafting Orb" ]

                Just orb ->
                    [ orb.icon.small, text orb.label ]
            )
        , div [ class "dropdown-menu" ]
            (View.Orb.list
                |> List.map
                    (\orb ->
                        button
                            [ type_ "button"
                            , class "dropdown-item btn btn-link"
                            , onClick <| SelectOrb index orb.enum
                            ]
                            [ orb.icon.small, text orb.label ]
                    )
            )
        ]
    , case selected of
        Nothing ->
            div [] []

        Just _ ->
            button [ type_ "button", class "btn", onClick <| RemoveOrb index ] [ code [] [ text "×" ] ]
    ]


type Msg
    = CriteriaMsg View.Criteria.Msg
    | InputIterations String
    | SelectOrb Int Orb
    | RemoveOrb Int
    | Run
    | RunGen Crafting.Orb.SpamArgs
    | RunSleep Crafting.Orb.SpamArgs
    | RunCancel
    | Noop
    | BaseMsg View.Item.Msg
    | SelectBaseItemGen Item


toCriteriaModel : Plan -> Model -> View.Criteria.Model
toCriteriaModel plan model =
    { vals = plan.criteria, search = model.criteriaSearch }


fromCriteriaModel : View.Criteria.Model -> Plan -> Model -> Model
fromCriteriaModel cm plan model =
    { model | criteriaSearch = cm.search, plan = Ok { plan | criteria = cm.vals } }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model.plan of
        Err err ->
            ( model, Cmd.none )

        Ok plan ->
            case msg of
                CriteriaMsg submsg ->
                    View.Criteria.update submsg (toCriteriaModel plan model)
                        |> Tuple.mapBoth (\c -> fromCriteriaModel c plan model) (Cmd.map CriteriaMsg)

                SelectOrb index orb ->
                    let
                        plan1 =
                            { plan
                                | recipe =
                                    if index >= List.length plan.recipe then
                                        plan.recipe ++ [ orb ]

                                    else
                                        plan.recipe |> List.Extra.setAt index orb
                            }
                    in
                    ( { model | plan = Ok plan1 }, Cmd.none )

                RemoveOrb index ->
                    ( { model | plan = Ok { plan | recipe = plan.recipe |> List.Extra.removeAt index } }, Cmd.none )

                InputIterations str ->
                    case String.toInt str of
                        Nothing ->
                            ( model, Cmd.none )

                        Just i ->
                            ( { model | plan = Ok { plan | iterations = i } }, Cmd.none )

                Run ->
                    ( model
                    , plan
                        |> Plan.encode
                        |> Just
                        |> Route.OrbSpam
                        |> Route.replaceUrl model.session.nav
                    )

                RunGen ({ progress } as spam) ->
                    case model.orbSpam of
                        Nothing ->
                            -- run was cancelled
                            ( model, Cmd.none )

                        Just _ ->
                            ( { model | orbSpam = Just spam.progress }
                            , if Crafting.Orb.isSpamDone progress then
                                Cmd.none

                              else
                                -- Need to sleep a little between random runs, so Elm redraws the view with current progress
                                Process.sleep 1 |> Task.perform (always <| RunSleep spam)
                            )

                RunSleep spam ->
                    case model.orbSpam of
                        Nothing ->
                            -- run was cancelled
                            ( model, Cmd.none )

                        Just _ ->
                            ( model, Random.generate RunGen <| Crafting.Orb.spam spam )

                RunCancel ->
                    ( { model | orbSpam = Nothing }, Cmd.none )

                BaseMsg (View.Item.RequestFocus id) ->
                    ( model, Task.attempt (always Noop) (Browser.Dom.focus id) )

                Noop ->
                    ( model, Cmd.none )

                BaseMsg (View.Item.InputSearchBaseItem search) ->
                    ( { model | baseItemSearch = Just search }, Cmd.none )

                BaseMsg (View.Item.SelectBaseItem base) ->
                    ( { model | baseItemSearch = Nothing }
                    , Crafting.Item.white model.session.datamine [] base |> Random.generate SelectBaseItemGen
                    )

                SelectBaseItemGen item0 ->
                    let
                        item =
                            { item0 | ilvl = plan.item.val.ilvl }
                    in
                    ( { model | plan = Ok { plan | item = View.ItemState.create model.session.datamine item } }, Cmd.none )

                BaseMsg (View.Item.InputInfluence inf) ->
                    let
                        item0 =
                            plan.item.val

                        item =
                            { item0 | influence = item0.influence |> BaseItem.influenceToggle inf }
                                |> View.ItemState.create model.session.datamine
                    in
                    ( { model | plan = Ok { plan | item = item } }, Cmd.none )

                BaseMsg (View.Item.InputItemLevel str) ->
                    case String.toInt str of
                        Nothing ->
                            ( model, Cmd.none )

                        Just ilvl ->
                            let
                                item0 =
                                    plan.item.val

                                item =
                                    { item0 | ilvl = ilvl }
                                        |> View.ItemState.create model.session.datamine
                            in
                            ( { model | plan = Ok { plan | item = item } }, Cmd.none )


run : Model -> Result String ( Model, Cmd Msg )
run model =
    case model.plan of
        Ok plan ->
            if isFormSubmittable plan model then
                let
                    gen : ItemState -> Generator Item
                    gen =
                        plan.recipe
                            |> List.map
                                (\orb ->
                                    Crafting.Orb.apply orb model.session.datamine
                                        |> Crafting.Orb.ignoreError
                                )
                            |> Crafting.Orb.chain

                    spam =
                        Crafting.Orb.spamArgs { batch = 1000, target = plan.iterations }
                            (View.Criteria.check model.session.datamine (toCriteriaModel plan model))
                            (gen plan.item)
                in
                Ok ( { model | orbSpam = Just spam.progress }, Random.generate RunGen <| Crafting.Orb.spam spam )

            else
                Err "not submittable"

        Err err ->
            Err err
