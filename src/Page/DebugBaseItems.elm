module Page.DebugBaseItems exposing (view)

import Datamine.BaseItem as BaseItem exposing (BaseItem)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import List.Extra
import Session exposing (Session)


view : Session -> List (Html msg)
view session =
    [ table []
        [ thead []
            [ tr []
                [ th [] [ text "id" ]
                , th [] [ text "icon" ]
                , th [] [ text "name" ]
                , th [] [ text "class" ]
                , th [] [ text "implicits" ]
                , th [] [ text "release" ]
                ]
            ]
        , tbody [] (session.datamine.baseItems |> List.map (viewBaseItem >> tr []))
        ]
    , session.datamine.baseItems
        |> List.map .itemClass
        |> List.Extra.unique
        |> List.map (\c -> "\"" ++ c ++ "\"")
        |> String.join ", "
        |> text
    ]


viewBaseItem : BaseItem -> List (Html msg)
viewBaseItem item =
    [ td [] [ text item.id ]
    , td [] [ img [ src <| BaseItem.img item ] [] ]
    , td [] [ text item.name ]
    , td [] [ text item.itemClass ]
    , td [] [ text <| String.join ", " item.implicits ]
    , td [] [ text item.releaseState ]
    ]
