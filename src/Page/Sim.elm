module Page.Sim exposing (Model, Msg, init, subscriptions, update, view)

import Browser.Dom
import Browser.Events
import Crafting.Item as Item exposing (Item)
import Crafting.Mod as Mod exposing (Mod)
import Crafting.Orb exposing (Orb, PossibleMods, SpamProgress)
import Datamine exposing (Datamine)
import Datamine.BaseItem as BaseItem exposing (BaseItem, Influence, InfluenceConfig)
import Datamine.BaseMod as BaseMod exposing (BaseMod, SpawnWeight)
import Datamine.Stat as Stat exposing (Stat)
import Dict exposing (Dict)
import Dict.Extra
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Icon
import Json.Decode as D exposing (Decoder)
import Json.Encode as JE
import List.Extra
import Maybe.Extra
import Plan
import Ports
import Process
import Random exposing (Generator)
import Result.Extra
import Route exposing (Route)
import Session exposing (Session)
import Set exposing (Set)
import Task
import View.Criteria exposing (CriteriaEntry)
import View.Item
import View.ItemState exposing (ItemState)
import View.ModGroup
import View.Nav
import View.Orb
import View.Util exposing (percent)


type alias Model =
    { session : Session
    , stateCache : Maybe String
    , item : Maybe ItemState
    , error : Maybe String
    , details : Bool
    , altKey : Bool
    , expanded : Set String
    , search : String
    }


init : Session -> Maybe String -> ( Model, Cmd Msg )
init session state =
    let
        decoded : Result String ItemState
        decoded =
            case state of
                Nothing ->
                    Item.default session.datamine
                        |> Result.map (View.ItemState.create session.datamine)

                Just s ->
                    View.ItemState.decode session.datamine s
    in
    ( { session = session
      , stateCache = state
      , item = decoded |> Result.toMaybe
      , error = decoded |> Result.Extra.error
      , details = True
      , altKey = False
      , expanded = Set.empty
      , search = ""
      }
    , Cmd.none
    )


view : Model -> List (Html Msg)
view model =
    [ div [ class "container" ]
        [ View.Nav.view { sim = Just <| Route.Sim model.stateCache, orbspam = Just <| orbspamRoute model }
        , case model.item of
            Nothing ->
                Icon.fas "spinner"

            Just item ->
                div []
                    (div [ class "row" ]
                        (viewCraftingRow model item)
                        :: viewModsRows model item
                    )
        ]
    ]


orbspamRoute : Model -> Route
orbspamRoute model =
    case model.item of
        Nothing ->
            Route.OrbSpam Nothing

        Just item ->
            item.val
                |> Plan.fromItem model.session.datamine
                |> Plan.encode
                |> Just
                |> Route.OrbSpam


viewCraftingRow : Model -> ItemState -> List (Html Msg)
viewCraftingRow model item =
    [ View.Item.view model item.val
        |> H.map BaseItemMsg
    , div [ class "col-sm" ]
        [ div [ class "form-check form-check-inline" ]
            [ input
                [ id "show-details"
                , type_ "checkbox"
                , checked model.details
                , onCheck <| always ToggleDetails
                ]
                []
            , label [ for "show-details", class "my-0" ] [ text "Show item details" ]
            ]
        , div []
            ((View.Orb.groups
                |> List.map (List.map (\orb -> orbButton model.session.datamine item orb.enum [ orb.icon.large, text orb.label ]))
             )
                ++ [ [ a [ class "btn btn-link", Route.href <| orbspamRoute model ] [ text "To Orb-spam >>" ] ] ]
                |> List.intersperse [ hr [] [] ]
                |> List.concat
            )

        -- , div [] [ a [ Route.href <| Route.Reroll { state = model.stateCache, item = item.val.base.name } ] [ text "Reroll item..." ] ]
        ]
    ]


viewModsRows : Model -> ItemState -> List (Html Msg)
viewModsRows model item =
    [ div [ class "row" ]
        [ div [ class "col-sm" ]
            [ div [ class "card mod-gentype-group" ]
                (View.ModGroup.view "Prefixes" model.session.datamine model.expanded item item.possibleModsScoured.normal.prefixes |> List.map (H.map ModGroupMsg))
            ]
        , div [ class "col-sm" ]
            [ div [ class "card mod-gentype-group" ]
                (View.ModGroup.view "Suffixes" model.session.datamine model.expanded item item.possibleModsScoured.normal.suffixes |> List.map (H.map ModGroupMsg))
            ]
        ]
    , div [ class "row" ]
        [ div [ class "col-sm" ]
            [ div [ class "card mod-gentype-group" ]
                (View.ModGroup.view "Bench Prefixes" model.session.datamine model.expanded item (List.map (Tuple.second >> Tuple.pair 0) item.possibleModsScoured.bench.prefixes) |> List.map (H.map BenchMsg))
            ]
        , div [ class "col-sm" ]
            [ div [ class "card mod-gentype-group" ]
                (View.ModGroup.view "Bench Suffixes" model.session.datamine model.expanded item (List.map (Tuple.second >> Tuple.pair 0) item.possibleModsScoured.bench.suffixes) |> List.map (H.map BenchMsg))
            ]
        ]
    , div [ class "row" ] <|
        if List.length item.possibleModsScoured.corruptions > 0 then
            [ div [ class "col-sm" ]
                [ div [ class "card mod-gentype-group" ]
                    (View.ModGroup.view "Corruptions" model.session.datamine model.expanded item item.possibleModsScoured.corruptions |> List.map (H.map ModGroupMsg))
                ]
            , div [ class "col-sm" ] []
            ]

        else
            []
    ]


orbButton : Datamine -> ItemState -> Orb -> List (Html Msg) -> Html Msg
orbButton dm item orb content =
    let
        attrs =
            case Crafting.Orb.apply orb dm item of
                Err err ->
                    [ disabled True, title err ]

                Ok gen ->
                    [ onClick <| CraftingOrb gen ]
    in
    div [] [ button (attrs ++ [ type_ "button", class "btn btn-outline-dark btn-crafting" ]) content ]


type Msg
    = ItemGen Item
    | CraftingOrb (Generator Item)
    | ToggleDetails
    | KeyEvent { isDown : Bool }
    | ModGroupMsg View.ModGroup.Msg
    | BenchMsg View.ModGroup.Msg
    | BaseItemMsg View.Item.Msg
    | Noop


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model.item of
        Nothing ->
            ( model, Cmd.none )

        Just item0 ->
            case msg of
                ItemGen item ->
                    -- ( { model | error = Nothing, item = model.item |> Maybe.map (updateItemState item) }, Cmd.none )
                    ( model
                    , item
                        |> Item.encode
                        |> Just
                        |> Route.Sim
                        |> Route.replaceUrl model.session.nav
                    )

                CraftingOrb gen ->
                    ( model, Random.generate ItemGen gen )

                ToggleDetails ->
                    ( { model | details = not model.details }, Cmd.none )

                KeyEvent e ->
                    ( { model | altKey = e.isDown }, Cmd.none )

                ModGroupMsg (View.ModGroup.ToggleGroup group) ->
                    ( { model | expanded = model.expanded |> toggle group }, Cmd.none )

                BenchMsg (View.ModGroup.ToggleGroup group) ->
                    ( { model | expanded = model.expanded |> toggle group }, Cmd.none )

                ModGroupMsg (View.ModGroup.SelectMod baseMod) ->
                    ( model, Cmd.none )

                BenchMsg (View.ModGroup.SelectMod baseMod) ->
                    let
                        orb =
                            Crafting.Orb.Bench baseMod
                    in
                    case Crafting.Orb.apply orb model.session.datamine item0 of
                        Err err ->
                            ( { model | error = Just err }, Cmd.none )

                        Ok gen ->
                            ( model, Random.generate ItemGen gen )

                BaseItemMsg (View.Item.RequestFocus id) ->
                    ( model, Task.attempt (always Noop) (Browser.Dom.focus id) )

                Noop ->
                    ( model, Cmd.none )

                BaseItemMsg (View.Item.InputSearchBaseItem search) ->
                    ( { model | search = search }, Cmd.none )

                BaseItemMsg (View.Item.SelectBaseItem base) ->
                    ( { model | search = "" }
                    , base
                        |> Item.white model.session.datamine []
                        |> Random.generate ItemGen
                    )

                BaseItemMsg (View.Item.InputInfluence inf) ->
                    ( { model
                        | item =
                            item0.val
                                |> (\i -> { i | influence = i.influence |> BaseItem.influenceToggle inf })
                                |> View.ItemState.create model.session.datamine
                                |> Just
                      }
                    , Cmd.none
                    )

                BaseItemMsg (View.Item.InputItemLevel str) ->
                    case String.toInt str of
                        Nothing ->
                            ( model, Cmd.none )

                        Just ilvl ->
                            ( { model
                                | item =
                                    item0.val
                                        |> (\i -> { i | ilvl = ilvl })
                                        |> View.ItemState.create model.session.datamine
                                        |> Just
                              }
                            , Cmd.none
                            )


toggle : comparable -> Set comparable -> Set comparable
toggle val set =
    if Set.member val set then
        Set.remove val set

    else
        Set.insert val set


subscriptions : Model -> Sub Msg
subscriptions _ =
    -- Alt seems to be a browser shortcut that removes focus? End result:
    -- [alt down > alt up > alt down] fails. Ports let us use preventDefault().
    Ports.modDetailsKey KeyEvent
