module Page.DebugStatEntry exposing (view)

import Datamine.Stat as Stat exposing (Stat)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Session exposing (Session)


view : Session -> List (Html msg)
view session =
    [ table []
        [ thead []
            [ tr []
                [ th [] [ text "ids" ]
                , th [] [ text "conditions_______" ]

                -- , th [] [ text "format" ]
                -- , th [] [ text "handler" ]
                , th [] [ text "string" ]
                ]
            ]
        , tbody []
            (session.datamine.statTranslations
                |> List.indexedMap
                    (\i stat ->
                        stat.en
                            |> List.indexedMap (viewStatEntry stat)
                            |> List.map
                                (tr
                                    [ class
                                        (if modBy 2 i == 0 then
                                            "even"

                                         else
                                            "odd"
                                        )
                                    ]
                                )
                    )
                |> List.concat
            )
        ]
    ]


viewStatEntry : Stat -> Int -> Stat.StatLangEntry -> List (Html msg)
viewStatEntry stat eindex entry =
    [ td [] [ a [ id <| "entry" ++ String.fromInt eindex ++ "__" ++ stat.id, href <| "#stat__" ++ stat.id ] [ text stat.id ] ]
    , td []
        [ ul [] (entry.vals |> List.indexedMap (\i v -> li [] [ text <| viewCondition i v.condition ]))
        ]

    --, td []
    --    [ ul [] (entry.vals |> List.map (\v -> li [] [ text v.format ]))
    --    ]
    --, td []
    --    [ ul [] (entry.vals |> List.map (\v -> li [] [ text <| String.join "," v.handler ]))
    --    ]
    , td [] [ pre [] [ text entry.string ] ]
    ]


viewCondition : Int -> Stat.Condition -> String
viewCondition index c =
    let
        n : String
        n =
            "n" ++ String.fromInt index

        condition =
            case
                ( Maybe.map String.fromInt c.min
                , Maybe.map String.fromInt c.max
                )
            of
                ( Nothing, Nothing ) ->
                    "_"

                ( Just min, Nothing ) ->
                    n ++ " >= " ++ min

                ( Just min, Just max ) ->
                    if min == max then
                        min ++ " == " ++ n

                    else
                        min ++ " <= " ++ n ++ " <= " ++ max

                ( Nothing, Just max ) ->
                    n ++ " <= " ++ max
    in
    condition
