module Page.DebugStat exposing (view)

import Datamine.Stat as Stat exposing (Stat)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Session exposing (Session)


view : Session -> List (Html msg)
view session =
    -- [ table [ attribute "border" "1" ]
    [ table []
        [ thead []
            [ tr []
                [ th [] [ text "ids" ]
                , th [] [ text "strings" ]
                ]
            ]
        , tbody [] (session.datamine.statTranslations |> List.map (viewStat >> tr []))
        ]
    ]


viewStat : Stat -> List (Html msg)
viewStat stat =
    [ td [] [ ul [] (stat.ids |> List.map (\id -> li [] [ a [ A.id <| "stat__" ++ stat.id, href <| "#entry0__" ++ stat.id ] [ text id ] ])) ]
    , td [] [ stat.en |> List.length |> String.fromInt |> text ]
    ]
