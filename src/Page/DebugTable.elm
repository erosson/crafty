module Page.DebugTable exposing (Model, init, view)

import Datamine.BaseItem as BaseItem exposing (BaseItem)
import Datamine.BaseMod as BaseMod exposing (BaseMod, ModStat)
import Datamine.CraftingBenchOption as CraftingBenchOption exposing (CraftingBenchOption)
import Datamine.Stat as Stat exposing (Stat, StatLang, StatLangEntry)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Maybe.Extra
import Session exposing (Session)


type alias Model =
    { session : Session, name : String }


init =
    Model


view : Model -> Maybe (List (Html msg))
view =
    viewContent
        >> Maybe.map
            (\data ->
                [ table []
                    [ thead [] [ tr [] (data.headers |> List.map (\h -> th [] [ text h ])) ]
                    , tbody [] (data.rows |> List.map (List.map (td []) >> tr []))
                    ]
                ]
            )


viewContent : Model -> Maybe { headers : List String, rows : List (List (List (Html msg))) }
viewContent model =
    case model.name of
        "crafting_bench_options" ->
            Just
                { headers =
                    [ "bench_group"
                    , "bench_tier"
                    , "mod_id"
                    , "mod_stats"
                    , "cost"
                    , "item_classes"
                    , "master"
                    ]
                , rows =
                    model.session.datamine.craftingBenchOptions
                        |> List.filter (\c -> c.master /= "Zana")
                        |> List.map
                            (\c ->
                                [ [ text c.benchGroup ]
                                , [ text <| String.fromInt c.benchTier ]
                                , [ text c.modId ]
                                , case CraftingBenchOption.mod model.session.datamine c of
                                    Nothing ->
                                        []

                                    Just mod ->
                                        [ ul [ style "white-space" "nowrap" ]
                                            (mod
                                                |> BaseMod.toStrings model.session.datamine
                                                |> List.map (\s -> li [] [ text s ])
                                            )
                                        ]
                                , [ ul [] (c.cost |> List.map (\( item, num ) -> li [] [ text item, text " x", text <| String.fromInt num ])) ]

                                -- , [ ul [] (c.itemClasses |> List.map (\i -> li [] [ text i ])) ]
                                , [ div [ style "width" "30em" ] [ text <| String.join ", " c.itemClasses ] ]
                                , [ text c.master ]
                                ]
                            )
                }

        _ ->
            Nothing
