module Page.Changelog exposing (view)

import Datamine.Stat as Stat exposing (Stat)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Markdown
import Session exposing (Session)
import View.Nav


view : Session -> List (Html msg)
view session =
    [ div [ class "container" ]
        [ View.Nav.view { sim = Nothing, orbspam = Nothing }
        , session.changelog
            |> String.split "---"
            |> List.drop 1
            |> String.join "---"
            |> Markdown.toHtml []
        ]
    ]
