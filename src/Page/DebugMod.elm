module Page.DebugMod exposing (view)

import Datamine exposing (Datamine)
import Datamine.BaseMod as BaseMod exposing (BaseMod)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Maybe.Extra
import Session exposing (Session)


view : Session -> List (Html msg)
view session =
    [ table []
        [ thead []
            [ tr []
                [ th [] [ text "id" ]
                , th [] [ text "name" ]
                , th [] [ text "generation_type" ]
                , th [] [ text "domain" ]
                , th [] [ text "stats" ]
                , th [] [ text "spawnWeights" ]
                , th [] [ text "translations" ]
                ]
            ]
        , tbody [] (session.datamine.mods |> List.map (viewMod session.datamine >> tr []))
        ]
    ]


viewMod : Datamine -> BaseMod -> List (Html msg)
viewMod dm mod =
    [ td [] [ text mod.id ]
    , td [] [ text mod.name ]
    , td [] [ text mod.generationType ]
    , td [] [ text mod.domain ]
    , td []
        [ ul []
            (mod.stats
                |> List.map
                    (\s ->
                        [ s.id
                        , ": "
                        , String.fromInt s.min
                        , "-"
                        , String.fromInt s.max
                        ]
                            |> List.map text
                            |> li []
                    )
            )
        ]
    , td [] [ ul [] (mod.spawnWeights |> List.map (\w -> li [] [ text w.tag, text ": ", text <| String.fromInt w.weight ])) ]
    , td []
        [ ul []
            (mod
                |> BaseMod.toStats dm
                |> List.map (\s -> s.en |> List.head |> Maybe.Extra.unwrap ("???" ++ s.id ++ "???") .string)
                |> List.map (\s -> li [] [ text s ])
            )
        ]
    ]
