// import './main.css';
import 'style-loader!css-loader!sass-loader!./main.scss'
// import '@fortawesome/fontawesome-free/css/all.css'
// import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
import { Elm } from './Main.elm';
import * as serviceWorker from './serviceWorker';
import stat_translations from '../third-party/RePoE/RePoE/data/stat_translations.json'
import mods from '../third-party/RePoE/RePoE/data/mods.json'
import base_items from '../third-party/RePoE/RePoE/data/base_items.json'
import crafting_bench_options from '../third-party/RePoE/RePoE/data/crafting_bench_options.json'
import changelog from '!!raw-loader!../CHANGELOG.md'

const app = Elm.Main.init({
  flags: {
    changelog,
    datamine: {
      stat_translations,
      mods,
      base_items,
      crafting_bench_options,
    },
  },
})

// Alt seems to be a browser shortcut that removes focus? End result:
// [alt down > alt up > alt down] fails. Ports let us use preventDefault().
const keyHandler = isDown => e => {
  if (e.key == 'Alt') {
    e.preventDefault()
    app.ports.modDetailsKey.send({isDown})
  }
}
document.addEventListener('keydown', keyHandler(true))
document.addEventListener('keyup', keyHandler(false))
// alt-tab makes alt "sticky" without this
window.addEventListener('focus', () =>
  keyHandler(false)({key: 'Alt', preventDefault: ()=>{}})
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
