module Session exposing (Flags, Session, init)

import Browser.Navigation as Nav
import Datamine exposing (Datamine)
import Json.Decode as D


type alias Flags =
    { datamine : D.Value
    , changelog : String
    }


type alias Session =
    { nav : Nav.Key
    , changelog : String
    , datamine : Datamine
    }


init : Flags -> Nav.Key -> Result String Session
init flags nav =
    Result.map (Session nav flags.changelog)
        (D.decodeValue Datamine.decoder flags.datamine
            |> Result.mapError D.errorToString
        )
