module Icon exposing (..)

import Html as H exposing (..)
import Html.Attributes as A exposing (..)


fas : String -> Html msg
fas name =
    span [ class "fas", class <| "fa-" ++ name ] []


src : String -> List (H.Attribute msg) -> Html msg
src s attrs =
    img ([ class "icon", A.src s ] ++ attrs) []


chaos =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyRerollRare.png?scale=1"


alteration =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyRerollMagic.png?scale=1"


alchemy =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyUpgradeToRare.png?scale=1"


transmutation =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyUpgradeToMagic.png?scale=1"


scouring =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyConvertToNormal.png?scale=1"


exalted =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyAddModToRare.png?scale=1"


regal =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyUpgradeMagicToRare.png?scale=1"


augmentation =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyAddModToMagic.png?scale=1"


divine =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyModValues.png?scale=1"


blessed =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyImplicitMod.png?scale=1"


annulment =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/AnnullOrb.png?scale=1"


vaal =
    src "https://web.poecdn.com/image/Art/2DItems/Currency/VaalOrb.png?scale=1"
