module Route exposing
    ( Route(..)
    , href
    , parse
    , pushUrl
    , replaceUrl
    )

import Browser.Navigation as Nav
import Dict exposing (Dict)
import Html as H
import Html.Attributes as A
import Url exposing (Url)
import Url.Builder as B
import Url.Parser as P exposing ((</>), (<?>), Parser)
import Url.Parser.Query as Q


type Route
    = Sim (Maybe String)
    | OrbSpam (Maybe String)
    | Changelog
    | DebugStat
    | DebugStatEntry
    | DebugMod
    | DebugBaseItems
    | DebugTable String


parse : Url -> Maybe Route
parse url =
    P.parse parser url


parser : Parser (Route -> a) a
parser =
    P.oneOf
        [ P.map Sim <| P.top <?> Q.string "item"
        , P.map OrbSpam <| P.s "orbspam" <?> Q.string "plan"
        , P.map Changelog <| P.s "changelog"
        , P.map DebugStat <| P.s "debug" </> P.s "stat"
        , P.map DebugStatEntry <| P.s "debug" </> P.s "statentry"
        , P.map DebugMod <| P.s "debug" </> P.s "mod"
        , P.map DebugBaseItems <| P.s "debug" </> P.s "baseitems"
        , P.map DebugTable <| P.s "debug" </> P.s "table" </> P.string
        ]


decodedString : Parser (String -> a) a
decodedString =
    P.map (\s -> Url.percentDecode s |> Maybe.withDefault s) P.string


toString : Route -> String
toString route =
    case route of
        OrbSpam plan ->
            let
                qs =
                    [ Maybe.map (B.string "plan") plan
                    ]
                        |> List.filterMap identity
                        |> B.toQuery
            in
            "/orbspam" ++ qs

        Sim item ->
            let
                qs =
                    [ Maybe.map (B.string "item") item
                    ]
                        |> List.filterMap identity
                        |> B.toQuery
            in
            "/" ++ qs

        Changelog ->
            "/changelog"

        DebugStat ->
            "/debug/stat"

        DebugStatEntry ->
            "/debug/statentry"

        DebugMod ->
            "/debug/mod"

        DebugBaseItems ->
            "/debug/baseitems"

        DebugTable t ->
            "/debug/table/" ++ t


href : Route -> H.Attribute msg
href =
    toString >> A.href


pushUrl : Nav.Key -> Route -> Cmd msg
pushUrl nav =
    toString >> Nav.pushUrl nav


replaceUrl : Nav.Key -> Route -> Cmd msg
replaceUrl nav =
    toString >> Nav.replaceUrl nav
