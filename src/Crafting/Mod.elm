module Crafting.Mod exposing
    ( DetailedStrings
    , Mod
    , Strings
    , decoder
    , encoder
    , gen
    , listToDetailedStrings
    , listToStrings
    , regen
    , statVals
    , toDetailedStrings
    , toStrings
    )

import Datamine exposing (Datamine)
import Datamine.BaseMod as BaseMod exposing (BaseMod, ModStat)
import Datamine.Stat as Stat exposing (Stat)
import Dict exposing (Dict)
import Json.Decode as D
import Json.Encode as E
import Random exposing (Generator)
import Random.Extra
import Set exposing (Set)


type alias Mod =
    { mod : BaseMod
    , vals : List Int
    }


gen : BaseMod -> Generator Mod
gen mod =
    mod.stats
        |> List.map (\stat -> Random.int stat.min stat.max)
        |> Random.Extra.combine
        |> Random.map (Mod mod)


regen : Mod -> Generator Mod
regen mod =
    gen mod.mod


type alias Strings =
    { stat : String
    , tags : Set String
    }


toStrings : Datamine -> Mod -> List Strings
toStrings dm mod =
    listToStrings dm [ mod ]


listToStrings : Datamine -> List Mod -> List Strings
listToStrings dm mods =
    listToStrings_ dm (mods |> List.filter (\m -> m.mod.domain /= "crafted")) Set.empty
        ++ listToStrings_ dm (mods |> List.filter (\m -> m.mod.domain == "crafted")) (Set.singleton "crafted")


listToStrings_ : Datamine -> List Mod -> Set String -> List Strings
listToStrings_ dm mods tags =
    let
        vals : Dict String Int
        vals =
            statVals dm mods
    in
    mods
        |> List.map .mod
        |> BaseMod.listToStats dm
        |> List.map (\s -> Stat.toStringVal s vals)
        |> List.filterMap
            (\r ->
                case r of
                    Err err ->
                        Nothing

                    Ok "" ->
                        Nothing

                    Ok stat ->
                        Just { stat = stat, tags = tags }
            )


type alias DetailedStrings =
    { label : String
    , stats : List String
    , tags : Set String
    }


listToDetailedStrings : Datamine -> List Mod -> List DetailedStrings
listToDetailedStrings dm =
    List.map (toDetailedStrings dm)


toDetailedStrings : Datamine -> Mod -> DetailedStrings
toDetailedStrings dm mod =
    let
        vals : Dict String Int
        vals =
            statVals dm [ mod ]

        tags =
            if mod.mod.domain == "crafted" then
                Set.singleton "crafted"

            else
                Set.empty

        head =
            if Set.member "crafted" tags then
                "Master crafted "

            else
                ""

        label : String
        label =
            let
                tail =
                    "\""
                        ++ mod.mod.name
                        ++ "\"; group \""
                        ++ mod.mod.group
                        ++ "\""
            in
            case mod.mod.generationType of
                "unique" ->
                    "Implicit modifier"

                "prefix" ->
                    head ++ "Prefix modifier " ++ tail

                "suffix" ->
                    head ++ "Suffix modifier " ++ tail

                _ ->
                    "Unknown modifier " ++ tail

        stats : List String
        stats =
            BaseMod.toStats dm mod.mod
                |> List.map (\s -> Stat.toStringVal s vals)
                |> List.filterMap
                    (\r ->
                        case r of
                            Err err ->
                                Nothing

                            Ok "" ->
                                Nothing

                            Ok ok ->
                                Just ok
                    )
    in
    { label = label, stats = stats, tags = tags }


statVals : Datamine -> List Mod -> Dict String Int
statVals dm =
    let
        fold : ( ModStat, Int ) -> Dict String Int -> Dict String Int
        fold ( stat, val ) =
            Dict.update stat.id
                (Maybe.withDefault 0
                    >> (+) val
                    >> Just
                )
    in
    List.concatMap (\m -> List.map2 Tuple.pair m.mod.stats m.vals)
        >> List.foldl fold Dict.empty


encoder : Mod -> E.Value
encoder mod =
    E.object
        [ ( "mod", mod.mod.id |> E.string )
        , ( "vals", mod.vals |> E.list E.int )
        ]


decoder : Datamine -> D.Decoder Mod
decoder dm =
    D.map2 Mod
        (D.field "mod" <| baseModDecoder dm)
        (D.field "vals" <| D.list D.int)


baseModDecoder : Datamine -> D.Decoder BaseMod
baseModDecoder dm =
    D.string
        |> D.andThen
            (\id ->
                case Dict.get id dm.modsById of
                    Nothing ->
                        D.fail <| "no such modid: " ++ id

                    Just mod ->
                        D.succeed mod
            )
