module Crafting.Item exposing
    ( Item
    , Magic(..)
    , affixes
    , decoder
    , default
    , encode
    , encoder
    , influenceToString
    , magicAffixes
    , toDetailedStrings
    , toStrings
    , white
    , whiteWithImplicits
    )

import Crafting.AffixWeight as AffixWeight exposing (AffixWeight)
import Crafting.Mod as Mod exposing (Mod)
import Datamine exposing (Datamine)
import Datamine.BaseItem as BaseItem exposing (BaseItem, Influence)
import Datamine.BaseMod as BaseMod exposing (BaseMod)
import Dict exposing (Dict)
import Json.Decode as D
import Json.Encode as E
import Maybe.Extra
import Random exposing (Generator)
import Random.Extra
import Set exposing (Set)


type alias Item =
    { base : BaseItem
    , ilvl : Int
    , influence : List Influence
    , implicits : List Mod
    , magic : Magic
    }


type Magic
    = White
      -- zero-mod magic and rare items are possible with orbs of annulment.
      -- It does *not* turn them white!
    | Blue (Maybe Mod) (Maybe Mod)
    | Yellow (List Mod) (List Mod)


whiteWithImplicits : List Influence -> List Mod -> BaseItem -> Item
whiteWithImplicits influence implicits base =
    { base = base
    , ilvl = 100
    , influence = influence
    , implicits = implicits
    , magic = White
    }


white : Datamine -> List Influence -> BaseItem -> Generator Item
white dm influence base =
    let
        baseImplicits : List BaseMod
        baseImplicits =
            base.implicits |> List.filterMap (\i -> Dict.get i dm.modsById)
    in
    Random.map (\implicits -> whiteWithImplicits influence implicits base)
        (baseImplicits |> List.map Mod.gen |> Random.Extra.combine)


default : Datamine -> Result String Item
default dm =
    -- divine life flask
    case Dict.get "Metadata/Items/Flasks/FlaskLife11" dm.baseItemsById of
        Nothing ->
            Err "default baseitem doesn't exist"

        Just base ->
            base
                |> whiteWithImplicits [] []
                |> Ok


affixes : Item -> List Mod
affixes item =
    item.implicits ++ magicAffixes item


magicAffixes : Item -> List Mod
magicAffixes { magic } =
    case magic of
        White ->
            []

        Blue pre suf ->
            [ pre, suf ] |> List.filterMap identity

        Yellow pre suf ->
            pre ++ suf


type alias Strings =
    { name : String
    , img : String
    , influence : String
    , implicits : List Mod.Strings
    , affixes : List Mod.Strings
    }


toStrings : Datamine -> Item -> Strings
toStrings dm item =
    { name =
        case item.magic of
            Blue a b ->
                magicName item.base a b

            _ ->
                -- TODO rare names
                item.base.name
    , img = BaseItem.img item.base
    , influence = ilvlInfluenceToString item
    , implicits = item.implicits |> Mod.listToStrings dm
    , affixes =
        case item.magic of
            White ->
                []

            Blue a b ->
                [ a, b ]
                    |> List.filterMap identity
                    |> Mod.listToStrings dm

            Yellow pre suf ->
                pre ++ suf |> Mod.listToStrings dm
    }


ilvlInfluenceToString : Item -> String
ilvlInfluenceToString item =
    "Level "
        ++ String.fromInt item.ilvl
        ++ (influenceToString item |> Maybe.Extra.unwrap "" ((++) ", "))


influenceToString : Item -> Maybe String
influenceToString item =
    case item.base.domain of
        "flask" ->
            Nothing

        _ ->
            case item.influence of
                [] ->
                    Just "no influence"

                _ ->
                    item.influence
                        |> List.map BaseItem.influenceToName
                        |> List.sort
                        |> String.join "/"
                        |> Just


type alias DetailedStrings =
    { name : String
    , img : String
    , influence : String
    , implicits : List Mod.DetailedStrings
    , affixes : List Mod.DetailedStrings
    }


toDetailedStrings : Datamine -> Item -> DetailedStrings
toDetailedStrings dm item =
    { name =
        case item.magic of
            Blue a b ->
                magicName item.base a b

            _ ->
                -- TODO rare names
                item.base.name
    , img = BaseItem.img item.base
    , influence = ilvlInfluenceToString item
    , implicits = item.implicits |> Mod.listToDetailedStrings dm
    , affixes =
        case item.magic of
            White ->
                []

            Blue a b ->
                [ a, b ]
                    |> List.filterMap identity
                    |> Mod.listToDetailedStrings dm

            Yellow pre suf ->
                pre ++ suf |> Mod.listToDetailedStrings dm
    }


magicName : BaseItem -> Maybe Mod -> Maybe Mod -> String
magicName base ma mb =
    [ ma |> Maybe.map (\a -> a.mod.name)
    , Just base.name
    , mb |> Maybe.map (\b -> b.mod.name)
    ]
        |> List.filterMap identity
        |> String.join " "


encode : Item -> String
encode =
    encoder >> E.encode 0


encoder : Item -> E.Value
encoder item =
    E.object
        [ ( "base", item.base.id |> E.string )
        , ( "ilvl", item.ilvl |> E.int )
        , ( "influence", item.influence |> BaseItem.influenceToNames |> Maybe.Extra.unwrap E.null E.string )
        , ( "implicits", item.implicits |> E.list Mod.encoder )
        , ( "magic", item.magic |> magicEncoder )
        ]


magicEncoder : Magic -> E.Value
magicEncoder m =
    E.object <|
        case m of
            White ->
                [ ( "rarity", E.string "normal" ) ]

            Blue p s ->
                [ ( "rarity", E.string "magic" )
                , ( "prefix", p |> Maybe.Extra.unwrap E.null Mod.encoder )
                , ( "suffix", s |> Maybe.Extra.unwrap E.null Mod.encoder )
                ]

            Yellow p s ->
                [ ( "rarity", E.string "rare" )
                , ( "prefixes", p |> E.list Mod.encoder )
                , ( "suffixes", s |> E.list Mod.encoder )
                ]


decoder : Datamine -> D.Decoder Item
decoder dm =
    D.map5 Item
        (D.field "base" <| baseItemDecoder dm)
        (D.field "ilvl" D.int)
        (D.field "influence" <| D.map (List.map .enum << BaseItem.influenceFromNames << Maybe.withDefault "") <| D.maybe D.string)
        (D.field "implicits" <| D.list (Mod.decoder dm))
        (D.field "magic" <| magicDecoder dm)


magicDecoder : Datamine -> D.Decoder Magic
magicDecoder dm =
    D.field "rarity" D.string
        |> D.andThen
            (\rarity ->
                case rarity of
                    "normal" ->
                        D.succeed White

                    "magic" ->
                        D.map2 Blue
                            (D.field "prefix" <| D.maybe (Mod.decoder dm))
                            (D.field "suffix" <| D.maybe (Mod.decoder dm))

                    "rare" ->
                        D.map2 Yellow
                            (D.field "prefixes" <| D.list (Mod.decoder dm))
                            (D.field "suffixes" <| D.list (Mod.decoder dm))

                    _ ->
                        D.fail <| "unknown rarity: " ++ rarity
            )


baseItemDecoder : Datamine -> D.Decoder BaseItem
baseItemDecoder dm =
    D.string
        |> D.andThen
            (\id ->
                case Dict.get id dm.baseItemsById of
                    Nothing ->
                        D.fail <| "unknown baseItem: " ++ id

                    Just base ->
                        D.succeed base
            )
