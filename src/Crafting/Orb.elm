module Crafting.Orb exposing
    ( Orb(..)
    , PossibleMods
    , SpamArgs
    , SpamProgress
    , apply
    , chain
    , decoder
    , encoder
    , filterMods
    , ignoreError
    , isSpamDone
    , list
    , possibleMods
    , spam
    , spamArgs
    , spamPercentDone
    , spamPercentPassed
    )

import Crafting.AffixWeight as AffixWeight exposing (AffixWeight)
import Crafting.Item as Item exposing (Item, Magic(..))
import Crafting.Mod as Mod exposing (Mod)
import Datamine exposing (Datamine)
import Datamine.BaseItem as BaseItem exposing (BaseItem, Influence, InfluenceConfig)
import Datamine.BaseMod as BaseMod exposing (BaseMod)
import Datamine.CraftingBenchOption as CraftingBenchOption exposing (CraftingBenchOption)
import Dict exposing (Dict)
import Dict.Extra
import Json.Decode as D
import Json.Encode as E
import List.Extra
import Maybe.Extra
import Random exposing (Generator)
import Random.Extra
import Random.List
import Set exposing (Set)


{-| All available crafting steps
-}
type Orb
    = Transmutation
    | Alteration
    | Augmentation
    | Regal
    | Alchemy
    | Chaos
    | Exalted
    | Blessed
    | Divine
    | Scouring
    | Annulment
    | Vaal
    | Bench BaseMod


list : List Orb
list =
    [ Transmutation
    , Alteration
    , Augmentation
    , Regal
    , Alchemy
    , Chaos
    , Exalted
    , Blessed
    , Divine
    , Scouring
    , Annulment

    -- , Vaal
    ]


config orb =
    case orb of
        Transmutation ->
            { enum = orb, fn = transmutation, name = "transmutation" }

        Alteration ->
            { enum = orb, fn = alteration, name = "alteration" }

        Augmentation ->
            { enum = orb, fn = augmentation, name = "augmentation" }

        Regal ->
            { enum = orb, fn = regal, name = "regal" }

        Alchemy ->
            { enum = orb, fn = alchemy, name = "alchemy" }

        Chaos ->
            { enum = orb, fn = chaos, name = "chaos" }

        Exalted ->
            { enum = orb, fn = exalted, name = "exalted" }

        Blessed ->
            { enum = orb, fn = blessed, name = "blessed" }

        Divine ->
            { enum = orb, fn = divine, name = "divine" }

        Scouring ->
            { enum = orb, fn = scouring, name = "scouring" }

        Annulment ->
            { enum = orb, fn = annulment, name = "annulment" }

        Vaal ->
            { enum = orb, fn = vaal, name = "vaal" }

        Bench mod ->
            { enum = orb, fn = bench mod, name = "bench" }


byName =
    list |> List.map config |> Dict.Extra.fromListBy .name


type alias ItemState =
    { val : Item
    , possibleModsScoured : PossibleMods
    , possibleModsCurrent : PossibleMods
    }


apply : Orb -> Datamine -> ItemState -> Result String (Generator Item)
apply orb =
    (config orb).fn


ignoreError : (ItemState -> Result String (Generator Item)) -> ItemState -> Generator Item
ignoreError fn item =
    case fn item of
        Err _ ->
            Random.constant item.val

        Ok gen ->
            gen


chain : List (ItemState -> Generator Item) -> ItemState -> Generator Item
chain fns item =
    case fns of
        [] ->
            Random.constant item.val

        head :: tail ->
            head item
                |> Random.map (\val -> { item | val = val, possibleModsCurrent = item.possibleModsScoured |> filterMods val })
                |> Random.andThen (chain tail)


transmutation : Datamine -> ItemState -> Result String (Generator Item)
transmutation dm ({ val } as item) =
    case val.magic of
        White ->
            rollAffixes dm item.possibleModsCurrent val AffixWeight.alteration
                |> Random.map (\( pre, suf ) -> { val | magic = Blue (List.head pre) (List.head suf) })
                |> Ok

        _ ->
            Err "Target is not Normal Rarity."


alteration : Datamine -> ItemState -> Result String (Generator Item)
alteration dm ({ val } as item) =
    case val.magic of
        Blue _ _ ->
            -- magic=white is important: alteration orb new-mods do not depend on old-mods
            transmutation dm { item | val = { val | magic = White } }

        _ ->
            Err "Target is not Magic."


augmentation : Datamine -> ItemState -> Result String (Generator Item)
augmentation dm ({ val } as item) =
    case item.val.magic of
        Blue Nothing Nothing ->
            -- technically possible with annuls. ugh.
            rollAffixes dm item.possibleModsCurrent val AffixWeight.oneAffix
                |> Random.map (\( pre, suf ) -> { val | magic = Blue (List.head pre) (List.head suf) })
                |> Ok

        Blue (Just a) Nothing ->
            rollAffixes dm item.possibleModsCurrent item.val AffixWeight.oneSuffix
                |> Random.map (\( pre, suf ) -> { val | magic = Blue (Just a) (List.head suf) })
                |> Ok

        Blue Nothing (Just b) ->
            rollAffixes dm item.possibleModsCurrent val AffixWeight.onePrefix
                |> Random.map (\( pre, suf ) -> { val | magic = Blue (List.head pre) (Just b) })
                |> Ok

        Blue (Just _) (Just _) ->
            Err "Item has no space for more Mods."

        _ ->
            Err "Target is not Magic."


alchemy : Datamine -> ItemState -> Result String (Generator Item)
alchemy dm ({ val } as item) =
    case val.magic of
        White ->
            if val.base.domain == "flask" then
                Err "Item cannot apply to the target item."

            else
                rollAffixes dm item.possibleModsCurrent val AffixWeight.chaos
                    |> Random.map (\( pre, suf ) -> { val | magic = Yellow pre suf })
                    |> Ok

        _ ->
            Err "Target is not Normal Rarity."


chaos : Datamine -> ItemState -> Result String (Generator Item)
chaos dm ({ val } as item) =
    case val.magic of
        Yellow _ _ ->
            -- magic=white is important: chaos orb new-mods do not depend on old-mods
            alchemy dm { item | val = { val | magic = White } }

        _ ->
            Err "Target is not Rare."


{-| Run an orb multiple times, testing its result against criteria as we go.

`spam` runs in multiple batches, since large runs will take a while.
It's done once `spam.progress.generated >= spam.progress.target`.

-}
spam : SpamArgs -> Generator SpamArgs
spam ({ criteria, orb, progress } as args) =
    let
        -- batch size will be zero when done, so no need to explicitly check if done
        batch : Int
        batch =
            args.progress.target
                - args.progress.generated
                |> min args.batch
                |> max 0
    in
    orb
        |> Random.map criteria
        |> List.repeat batch
        |> Random.Extra.combine
        |> Random.map
            (\passes ->
                { progress
                    | generated = progress.generated + batch
                    , passed = progress.passed + List.Extra.count identity passes
                }
            )
        |> Random.map (\p -> { args | progress = p })


type alias SpamArgs =
    { progress : SpamProgress, criteria : Item -> Bool, batch : Int, orb : Generator Item }


type alias SpamProgress =
    { target : Int, generated : Int, passed : Int }


spamArgs : { target : Int, batch : Int } -> (Item -> Bool) -> Generator Item -> SpamArgs
spamArgs { target, batch } criteria orb =
    { progress = SpamProgress target 0 0, criteria = criteria, batch = batch, orb = orb }


spamPercentDone : SpamProgress -> Float
spamPercentDone p =
    toFloat p.generated / toFloat p.target


spamPercentPassed : SpamProgress -> Float
spamPercentPassed p =
    toFloat p.passed / toFloat p.generated


isSpamDone : SpamProgress -> Bool
isSpamDone p =
    p.generated >= p.target


scouring : Datamine -> ItemState -> Result String (Generator Item)
scouring dm ({ val } as item) =
    case val.magic of
        White ->
            Err "Target is not Magic or Rare."

        _ ->
            Ok <| Random.constant { val | magic = White }


blessed : Datamine -> ItemState -> Result String (Generator Item)
blessed dm ({ val } as item) =
    -- sometimes implicits are invisible, with no stats. ex. life flasks have a level requirement implicit
    case val.implicits |> List.concatMap (.mod >> .stats) of
        [] ->
            Err "Could not reroll target's implicit Mod values."

        _ ->
            val.implicits
                |> List.map Mod.regen
                |> Random.Extra.combine
                |> Random.map (\is -> { val | implicits = is })
                |> Ok


divine : Datamine -> ItemState -> Result String (Generator Item)
divine dm ({ val } as item) =
    -- TODO cannot modify prefixes/suffixes affects divining
    case val.magic of
        White ->
            Err "Target has no mods to modify."

        Yellow pre suf ->
            Random.map2 (\p s -> { val | magic = Yellow p s })
                (pre |> List.map Mod.regen |> Random.Extra.combine)
                (suf |> List.map Mod.regen |> Random.Extra.combine)
                |> Ok

        Blue pre suf ->
            Random.map2 (\p s -> { val | magic = Blue p s })
                (pre |> Maybe.map Mod.regen |> randomCombineMaybe)
                (suf |> Maybe.map Mod.regen |> randomCombineMaybe)
                |> Ok


annulment : Datamine -> ItemState -> Result String (Generator Item)
annulment dm ({ val } as item) =
    case val.magic of
        White ->
            Err "Target is not Magic or Rare."

        Blue (Just p) (Just s) ->
            Random.Extra.choice p s
                |> Random.map
                    (\c ->
                        if c == p then
                            Blue (Just p) Nothing

                        else
                            Blue Nothing (Just s)
                    )
                |> Random.map (\magic -> { val | magic = magic })
                |> Ok

        Blue Nothing Nothing ->
            Err "Item cannot apply to the target item."

        Blue _ _ ->
            -- if there's exactly one magic mod, I don't care whether it's a prefix or suffix - it's gone.
            -- This makes the item a zero-mod magic item, not a white item!
            { val | magic = Blue Nothing Nothing }
                |> Random.constant
                |> Ok

        Yellow pre0 suf0 ->
            case pre0 ++ suf0 of
                [] ->
                    -- no mods left to annul!
                    Err "Item cannot apply to the target item."

                affs0 ->
                    Random.List.choose affs0
                        |> Random.map
                            -- drop the randomly chosen mod, keep the other mods
                            (\( _, affs ) ->
                                let
                                    ( pre, suf ) =
                                        affs |> List.partition (\m -> m.mod.generationType == "prefix")
                                in
                                { val | magic = Yellow pre suf }
                            )
                        |> Ok


regal : Datamine -> ItemState -> Result String (Generator Item)
regal dm ({ val } as item) =
    case val.magic of
        Blue pre0 suf0 ->
            if val.base.domain == "flask" then
                Err "Item cannot apply to the target item."

            else
                rollAffixes dm item.possibleModsCurrent val AffixWeight.oneAffix
                    |> Random.map (\( pre1, suf1 ) -> { val | magic = Yellow (Maybe.Extra.toList pre0 ++ pre1) (Maybe.Extra.toList suf0 ++ suf1) })
                    |> Ok

        _ ->
            Err "Target is not Magic."


exalted : Datamine -> ItemState -> Result String (Generator Item)
exalted dm ({ val } as item) =
    case val.magic of
        Yellow pre0 suf0 ->
            let
                aw =
                    if List.length pre0 >= 3 then
                        if List.length suf0 >= 3 then
                            -- neither prefixes nor suffixes have room
                            Err "Item has no space for more Mods."

                        else
                            -- suffixes have room, prefixes don't
                            Ok AffixWeight.oneSuffix

                    else if List.length suf0 >= 3 then
                        -- prefixes have room, suffixes don't
                        Ok AffixWeight.onePrefix

                    else
                        -- prefixes and suffixes both have room
                        Ok AffixWeight.oneAffix
            in
            Result.map
                (rollAffixes dm item.possibleModsCurrent val
                    >> Random.map (\( pre1, suf1 ) -> { val | magic = Yellow (pre0 ++ pre1) (suf0 ++ suf1) })
                )
                aw

        _ ->
            Err "Target is not Magic or Rare."


vaal : Datamine -> ItemState -> Result String (Generator Item)
vaal dm item =
    -- TODO detect already-corrupted
    if item.val.base.domain == "flask" then
        Err "Item cannot be corrupted."

    else
        -- TODO
        Ok <| Random.constant item.val


bench : BaseMod -> Datamine -> ItemState -> Result String (Generator Item)
bench baseMod dm ({ val } as item) =
    if Set.member baseMod.id item.possibleModsCurrent.ids then
        case val.magic of
            Item.White ->
                Err "TODO bench white"

            Item.Blue pre suf ->
                case baseMod.generationType of
                    "prefix" ->
                        if Maybe.Extra.isJust pre then
                            Err "TODO no mod space"

                        else
                            Mod.gen baseMod
                                |> Random.map (\mod -> { val | magic = Blue (Just mod) suf })
                                |> Ok

                    "suffix" ->
                        if Maybe.Extra.isJust suf then
                            Err "TODO no mod space"

                        else
                            Mod.gen baseMod
                                |> Random.map (\mod -> { val | magic = Blue pre (Just mod) })
                                |> Ok

                    _ ->
                        Err "TODO weird generationType"

            Item.Yellow pres sufs ->
                case baseMod.generationType of
                    "prefix" ->
                        if List.length pres >= 3 then
                            Err "TODO no mod space"

                        else
                            Mod.gen baseMod
                                |> Random.map (\mod -> { val | magic = Yellow (mod :: pres) sufs })
                                |> Ok

                    "suffix" ->
                        if List.length sufs >= 3 then
                            Err "TODO no mod space"

                        else
                            Mod.gen baseMod
                                |> Random.map (\mod -> { val | magic = Yellow pres (mod :: sufs) })
                                |> Ok

                    _ ->
                        Err "TODO weird generationType"

    else
        Err "Item already has a mod of this type."



--- Common utilities


type alias PossibleMods =
    { normal :
        { prefixes : List ( Float, BaseMod )
        , suffixes : List ( Float, BaseMod )
        }
    , bench :
        { prefixes : List ( CraftingBenchOption, BaseMod )
        , suffixes : List ( CraftingBenchOption, BaseMod )
        }
    , corruptions : List ( Float, BaseMod )
    , ids : Set String
    }


{-| All possible mods for this empty baseitem + influence combo.

Cache this and reuse it on each orb call for this baseitem + influence.

-}
possibleMods : Datamine -> List Influence -> Int -> BaseItem -> PossibleMods
possibleMods dm influence ilvl base =
    let
        -- this is slow; we're iterating every known mod!
        mods : List ( Float, BaseMod )
        mods =
            base
                |> BaseItem.possibleMods dm (influence |> List.map BaseItem.influenceConfig) ilvl
                |> List.map (\( mod, w ) -> ( toFloat w.weight, mod ))

        benchMods : List ( CraftingBenchOption, BaseMod )
        benchMods =
            base |> BaseItem.benchMods dm
    in
    { normal =
        { prefixes = mods |> List.filter (\( _, m ) -> m.generationType == "prefix")
        , suffixes = mods |> List.filter (\( _, m ) -> m.generationType == "suffix")
        }
    , bench =
        { prefixes = benchMods |> List.filter (\( _, m ) -> m.generationType == "prefix")
        , suffixes = benchMods |> List.filter (\( _, m ) -> m.generationType == "suffix")
        }
    , corruptions = mods |> List.filter (\( _, m ) -> m.generationType == "corrupted")
    , ids =
        [ mods |> List.map (Tuple.second >> .id)
        , benchMods |> List.map (Tuple.second >> .id)
        ]
            |> List.concat
            |> Set.fromList
    }


filterMods : Item -> PossibleMods -> PossibleMods
filterMods item p =
    let
        affixes =
            Item.magicAffixes item

        benchAffixes =
            affixes |> List.filter (\a -> a.mod.domain == "crafted")

        filterw =
            Tuple.second >> isModsCompatible affixes

        filterb =
            if List.length benchAffixes <= 1 then
                Tuple.second >> isModsCompatible affixes

            else
                always True

        raw =
            { normal =
                { prefixes = List.filter filterw p.normal.prefixes
                , suffixes = List.filter filterw p.normal.suffixes
                }
            , bench =
                { prefixes = List.filter filterb p.bench.prefixes
                , suffixes = List.filter filterb p.bench.suffixes
                }
            , corruptions = List.filter filterw p.corruptions
            }
    in
    { normal = raw.normal
    , bench = raw.bench
    , corruptions = raw.corruptions
    , ids =
        [ [ raw.normal.prefixes, raw.normal.suffixes, raw.corruptions ] |> List.concat |> List.map (Tuple.second >> .id)
        , [ raw.bench.prefixes, raw.bench.suffixes ] |> List.concat |> List.map (Tuple.second >> .id)
        ]
            |> List.concat
            |> Set.fromList
    }


weighted : List ( Float, a ) -> Generator (Maybe a)
weighted mods =
    case mods of
        [] ->
            Random.constant Nothing

        head :: tail ->
            Random.weighted head tail |> Random.map Just


{-| Like Random.Extra.combine for lists, but for maybes
-}
randomCombineMaybe : Maybe (Generator a) -> Generator (Maybe a)
randomCombineMaybe ma =
    case ma of
        Nothing ->
            Random.constant Nothing

        Just gen ->
            gen |> Random.map Just


isModCompatible : Mod -> BaseMod -> Bool
isModCompatible mod base =
    mod.mod.group /= base.group


isModsCompatible : List Mod -> BaseMod -> Bool
isModsCompatible mods base =
    case mods of
        [] ->
            True

        head :: tail ->
            isModCompatible head base && isModsCompatible tail base


type alias ModRoller =
    { selected : List Mod, num : Int, possible : List ( Float, BaseMod ) }


rollAffixes : Datamine -> PossibleMods -> Item -> Generator AffixWeight -> Generator ( List Mod, List Mod )
rollAffixes dm possible item =
    Random.andThen
        (\num ->
            rollMods_
                [ ModRoller [] num.prefixes possible.normal.prefixes
                , ModRoller [] num.suffixes possible.normal.suffixes
                ]
                |> Random.map
                    (\rs ->
                        case rs of
                            [ pre, suf ] ->
                                ( pre, suf )

                            -- rollMods guarantees the same number of inputs, so this should not be possible
                            _ ->
                                ( [], [] )
                    )
        )


{-| Roll multiple collections of mods in random order

In practice, this only rolls two collections, prefixes and suffixes; or a single
collection. It's convenient to have the same code support both though.

-}
rollMods_ : List ModRoller -> Generator (List (List Mod))
rollMods_ =
    let
        loop : List ( Int, ModRoller ) -> Generator (List ( Int, List Mod ))
        loop rollers =
            let
                -- ignore collections where we've spawned all requested mods
                ( pending, done ) =
                    rollers |> List.partition (\( i, r ) -> r.num > 0)
            in
            Random.List.choose pending
                |> Random.andThen
                    (\( mchosen, others ) ->
                        case mchosen of
                            -- all collections are done, exit. `others` will be empty
                            Nothing ->
                                others
                                    ++ done
                                    |> List.map (Tuple.mapSecond .selected)
                                    |> Random.constant

                            Just roller ->
                                -- we chose a collection, roll its next mod
                                rollOneAndFilter roller others
                                    |> Random.andThen (\rs -> loop <| rs ++ done)
                    )
    in
    List.indexedMap Tuple.pair >> loop >> Random.map (List.sortBy Tuple.first >> List.map Tuple.second)


rollOneAndFilter : ( Int, ModRoller ) -> List ( Int, ModRoller ) -> Generator (List ( Int, ModRoller ))
rollOneAndFilter ( ri, roller0 ) others0 =
    rollOne roller0
        |> Random.map
            (\( mmod, roller1 ) ->
                ( ri, roller1 )
                    :: (case mmod of
                            Nothing ->
                                -- no mods were left to roll. uncommon
                                others0

                            Just mod ->
                                -- rolled a mod, filter its group from all possible mod collections
                                others0 |> List.map (Tuple.mapSecond (\r -> { r | possible = r.possible |> List.filter (Tuple.second >> isModCompatible mod) }))
                       )
            )


rollOne : ModRoller -> Generator ( Maybe Mod, ModRoller )
rollOne r =
    case r.possible of
        [] ->
            Random.constant ( Nothing, { r | num = 0 } )

        head :: tail ->
            Random.weighted head tail
                |> Random.andThen Mod.gen
                |> Random.map
                    (\mod ->
                        ( Just mod
                        , { r
                            | num = r.num - 1
                            , possible = r.possible |> List.filter (Tuple.second >> isModCompatible mod)
                            , selected = mod :: r.selected
                          }
                        )
                    )


encoder : Orb -> E.Value
encoder =
    config >> .name >> E.string


decoder : D.Decoder Orb
decoder =
    D.string
        |> D.andThen
            (\name ->
                case Dict.get name byName of
                    Nothing ->
                        D.fail <| "no such orb: " ++ name

                    Just orb ->
                        D.succeed orb.enum
            )
