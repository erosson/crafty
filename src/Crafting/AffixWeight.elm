module Crafting.AffixWeight exposing (AffixWeight, alteration, chaos, chaosJewel, oneAffix, onePrefix, oneSuffix)

{-| Some crafting methods apply a random number of affixes. How are they weighted?

I haven't collected my own data, and affix-counts can't be datamined AFAIK.
Cribbed it all from these sources:

  - <https://www.reddit.com/r/pathofexile/comments/ex2c5o/data_number_of_mods_acquired_from_rerolling_an/>
  - <https://www.reddit.com/r/pathofexile/comments/8fxnlu/chance_of_getting_specific_number_of_mods_via/>

Fossils and essences use the same affix-count weighting as chaos orbs here.
Again, haven't collected my own data, could be wrong.

I assume prefix vs. suffix is always a 50/50 chance, regardless of the relative
total weight of prefix-mods and suffix-mods. That is, weight only matters when
choosing among prefixes or among suffixes; prefix vs. suffix is assumed to be a
separate 50/50 roll.

If there are no affixes left to choose from (perhaps metamods and fossils reduce
the pool to two modgroups? Can that happen?), this tool simply omits that affix.
I'm not sure if that's how PoE handles it - could leave it out, could reroll as
the other kind of affix, could error and not perform the craft?

TODO: implement crafting. a few different approaches:

  - run a single random roll (alt, alt+aug, chaos, fossil, essence...) and display the result
      - this is not new
  - set a stat/mod filter, run many (1000+) random rolls, and display how many passed the filter
      - with deduping? or dupe-reports?
      - this is novel, and very useful, and not difficult to implement!
  - set a stat/mod filter, do math to predict the probability of an item passing the filter
      - this is novel, and very useful - but tricky to implement

-}

import Random exposing (Generator)


type alias AffixWeight =
    { prefixes : Int
    , suffixes : Int
    , weight : Float
    }


gen : AffixWeight -> List AffixWeight -> Generator AffixWeight
gen head tail =
    Random.weighted ( head.weight, head ) (List.map (\t -> ( t.weight, t )) tail)


alteration : Generator AffixWeight
alteration =
    gen
        -- 1-mod: 50%
        (AffixWeight 0 1 25)
        [ AffixWeight 1 0 25

        -- 2-mod: 50%
        , AffixWeight 1 1 50
        ]


{-| Alteration + augmentation, for convenience.

One-mod magic items are rarely useful. (Exception: flasks + beastcrafting)
Augmentation orbs are cheap and abundant. It's often convenient to see the odds
of alteration + augmentation as if it were a single step.

-}
altAug : Generator AffixWeight
altAug =
    Random.constant <| AffixWeight 1 1 0


{-| Jewels are capped at 4 mods, so chaos orbs behave differently
-}
chaosJewel : Generator AffixWeight
chaosJewel =
    gen
        -- 3-mod: 2/3
        (AffixWeight 1 2 100)
        [ AffixWeight 2 1 100

        -- 4-mod: 1/3
        , AffixWeight 2 2 100
        ]


chaos : Generator AffixWeight
chaos =
    gen
        -- 4-mod: 8/12
        (AffixWeight 1 3 200)
        [ AffixWeight 2 2 400
        , AffixWeight 3 1 200

        -- 5-mod: 3/12
        , AffixWeight 2 3 150
        , AffixWeight 3 2 150

        -- 6-mod: 1/12
        , AffixWeight 3 3 100
        ]


onePrefix : Generator AffixWeight
onePrefix =
    Random.constant <| AffixWeight 1 0 0


oneSuffix : Generator AffixWeight
oneSuffix =
    Random.constant <| AffixWeight 0 1 0


oneAffix : Generator AffixWeight
oneAffix =
    gen
        (AffixWeight 1 0 50)
        [ AffixWeight 0 1 50 ]
