module View.Criteria exposing (CriteriaEntry, Model, Msg, check, decode, decoder, encode, encoder, init, update, view)

{-| Item filter criteria.

Define stat-based item filters that an item may pass or fail. Like on trade sites.

-}

import Browser.Dom
import Crafting.Item as Item exposing (Item)
import Crafting.Mod as Mod exposing (Mod)
import Crafting.Orb exposing (PossibleMods)
import Datamine exposing (Datamine)
import Datamine.BaseMod as BaseMod exposing (BaseMod, SpawnWeight)
import Datamine.Stat as Stat exposing (Stat)
import Dict exposing (Dict)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Json.Decode as D exposing (Decoder)
import Json.Encode as JE
import List.Extra
import Maybe.Extra
import Random exposing (Generator)
import Task


type alias Model =
    { vals : List CriteriaEntry
    , search : Maybe String
    }


type alias CriteriaEntry =
    { stat : Stat
    , min : Maybe Int
    , max : Maybe Int
    }


init : Datamine -> Maybe String -> Model
init dm arg =
    { vals = Maybe.Extra.unwrap [] (decode dm) arg
    , search = Nothing
    }


check : Datamine -> Model -> Item -> Bool
check dm model item =
    let
        stats : Dict String Int
        stats =
            -- TODO pseudomods
            item |> Item.affixes |> Mod.statVals dm

        check1 : CriteriaEntry -> Bool
        check1 c =
            -- TODO: some stats have more than one numeric value.
            -- We only check the first here. Should do something about that.
            case Dict.get c.stat.id stats of
                Nothing ->
                    False

                Just val ->
                    case ( c.min, c.max ) of
                        ( Nothing, Nothing ) ->
                            True

                        ( Nothing, Just max ) ->
                            val <= max

                        ( Just min, Just max ) ->
                            val >= min && val <= max

                        ( Just min, Nothing ) ->
                            val >= min
    in
    List.all check1 model.vals


view : Datamine -> Model -> PossibleMods -> List (Html Msg)
view dm model possibleMods =
    let
        unfilteredStats : List Stat
        unfilteredStats =
            possibleMods.normal.prefixes
                ++ possibleMods.normal.suffixes
                |> List.map Tuple.second
                |> BaseMod.listToStats dm

        search =
            model.search |> Maybe.withDefault "" |> String.toLower

        stats : List ( Stat, String )
        stats =
            unfilteredStats
                |> List.filterMap (\s -> Stat.toStringDefault s |> Maybe.map (Tuple.pair s))
                |> List.filter (\( stat, str ) -> String.contains search (String.toLower str))
                |> List.take 10
    in
    [ ul [ class "filter-criteria pl-0" ]
        (li [ class "dropdown py-0" ]
            [ button
                -- type=button avoids form submits
                [ type_ "button"
                , class "btn btn-outline-dark dropdown-toggle"
                , A.attribute "data-toggle" "dropdown"

                -- https://stackoverflow.com/questions/45959649/make-bootstrap-dropdown-menu-go-only-downwards
                -- , A.property "data-dropup-auto" (JE.bool False)
                , A.attribute "data-dropup-auto" "false"
                , onClick <| RequestFocus "criteria-search"
                ]
                [ text "+ Add Stat Filter" ]
            , div [ class "dropdown-menu" ]
                (div [ class "dropdown-item" ] [ input [ class "form-control", id "criteria-search", onInput SearchNextCriteria ] [] ]
                    :: (stats
                            |> List.map
                                (\( stat, str ) ->
                                    button
                                        [ type_ "button"
                                        , class "dropdown-item btn btn-link"
                                        , onClick <| SelectNextCriteria stat
                                        ]
                                        (str |> String.lines |> List.map (\l -> div [] [ text l ]))
                                )
                       )
                )
            ]
            :: (model.vals
                    |> List.indexedMap
                        (\i c ->
                            li [ class "py-0 my-0" ]
                                [ div [ class "p-0 m-0 form-group form-inline" ]
                                    ([ [ label [ style "display" "inline-block" ]
                                            (c.stat
                                                |> Stat.toStringDefault
                                                |> Maybe.withDefault ""
                                                |> String.lines
                                                |> List.map (\l -> div [] [ text l ])
                                            )
                                       ]
                                     , if Stat.numValues c.stat > 0 then
                                        [ input
                                            [ class "filter-criteria-minmax form-control"

                                            -- , style "display" "inline"
                                            -- , type_ "number"
                                            , placeholder "min"
                                            , onInput <| CriteriaInputMin i
                                            , value <| Maybe.Extra.unwrap "" String.fromInt c.min
                                            ]
                                            []
                                        , input
                                            [ class "filter-criteria-minmax form-control"

                                            -- , style "display" "inline"
                                            -- , type_ "number"
                                            , placeholder "max"
                                            , onInput <| CriteriaInputMax i
                                            , value <| Maybe.Extra.unwrap "" String.fromInt c.max
                                            ]
                                            []
                                        ]

                                       else
                                        []
                                     , [ button
                                            [ type_ "button"
                                            , class "btn filter-criteria-remove"
                                            , onClick <| CriteriaInputRemove i
                                            ]
                                            [ text "×" ]
                                       ]
                                     ]
                                        |> List.concat
                                    )
                                ]
                        )
               )
            |> List.reverse
        )
    ]


type Msg
    = SearchNextCriteria String
    | SelectNextCriteria Stat
    | CriteriaInputMin Int String
    | CriteriaInputMax Int String
    | CriteriaInputRemove Int
    | RequestFocus String
    | Noop


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SearchNextCriteria search ->
            ( { model | search = Just search }, Cmd.none )

        SelectNextCriteria stat ->
            ( { model | search = Nothing, vals = CriteriaEntry stat Nothing Nothing :: model.vals }, Cmd.none )

        CriteriaInputMin index str ->
            if str == "" then
                ( { model | vals = model.vals |> List.Extra.updateAt index (\c -> { c | min = Nothing }) }, Cmd.none )

            else
                case String.toInt str of
                    Nothing ->
                        ( model, Cmd.none )

                    Just val ->
                        ( { model | vals = model.vals |> List.Extra.updateAt index (\c -> { c | min = Just val }) }, Cmd.none )

        CriteriaInputMax index str ->
            if str == "" then
                ( { model | vals = model.vals |> List.Extra.updateAt index (\c -> { c | max = Nothing }) }, Cmd.none )

            else
                case String.toInt str of
                    Nothing ->
                        ( model, Cmd.none )

                    Just val ->
                        ( { model | vals = model.vals |> List.Extra.updateAt index (\c -> { c | max = Just val }) }, Cmd.none )

        CriteriaInputRemove index ->
            ( { model | vals = model.vals |> List.Extra.removeAt index }, Cmd.none )

        RequestFocus id ->
            ( model, Task.attempt (always Noop) (Browser.Dom.focus id) )

        Noop ->
            ( model, Cmd.none )


encoder : CriteriaEntry -> JE.Value
encoder crit =
    [ Just ( "stat", crit.stat.id |> JE.string )
    , crit.min |> Maybe.map (\m -> ( "min", m |> JE.int ))
    , crit.max |> Maybe.map (\m -> ( "max", m |> JE.int ))
    ]
        |> List.filterMap identity
        |> JE.object


decoder : Datamine -> D.Decoder CriteriaEntry
decoder dm =
    D.map3 CriteriaEntry
        (D.field "stat" <| statDecoder dm)
        (D.maybe <| D.field "min" D.int)
        (D.maybe <| D.field "max" D.int)


statDecoder : Datamine -> D.Decoder Stat
statDecoder dm =
    D.string
        |> D.andThen
            (\id ->
                case Dict.get id dm.statsById of
                    Nothing ->
                        D.fail <| "no such stat: " ++ id

                    Just stat ->
                        D.succeed stat
            )


encode : Model -> Maybe String
encode model =
    if model.vals == [] then
        Nothing

    else
        model.vals
            |> JE.list encoder
            |> JE.encode 0
            |> Just


decode : Datamine -> String -> List CriteriaEntry
decode dm c =
    case D.decodeString (D.list <| decoder dm) c of
        Err err ->
            []

        Ok ok ->
            ok
