module View.Orb exposing (OrbView, groups, list, orbView)

import Crafting.Orb exposing (Orb(..))
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Icon


type alias OrbView msg =
    { enum : Orb
    , icon : Icons msg
    , label : String
    }


type alias Icons msg =
    { fn : List (H.Attribute msg) -> Html msg
    , large : Html msg
    , small : Html msg
    }


icons fn =
    Icons fn (fn []) (fn [ class "icon-small" ])


orbView : Orb -> OrbView msg
orbView orb =
    case orb of
        Transmutation ->
            { enum = orb, icon = icons Icon.transmutation, label = "Orb of Transmutation" }

        Alteration ->
            { enum = orb, icon = icons Icon.alteration, label = "Orb of Alteration" }

        Augmentation ->
            { enum = orb, icon = icons Icon.augmentation, label = "Orb of Augmentation" }

        Regal ->
            { enum = orb, icon = icons Icon.regal, label = "Regal Orb" }

        Alchemy ->
            { enum = orb, icon = icons Icon.alchemy, label = "Orb of Alchemy" }

        Chaos ->
            { enum = orb, icon = icons Icon.chaos, label = "Chaos Orb" }

        Exalted ->
            { enum = orb, icon = icons Icon.exalted, label = "Exalted Orb" }

        Blessed ->
            { enum = orb, icon = icons Icon.blessed, label = "Blessed Orb" }

        Divine ->
            { enum = orb, icon = icons Icon.divine, label = "Divine Orb" }

        Scouring ->
            { enum = orb, icon = icons Icon.scouring, label = "Orb of Scouring" }

        Annulment ->
            { enum = orb, icon = icons Icon.annulment, label = "Orb of Annulment" }

        Vaal ->
            { enum = orb, icon = icons Icon.vaal, label = "Vaal Orb" }

        Bench _ ->
            { enum = orb, icon = icons Icon.augmentation, label = "Benchcraft (TODO)" }


groups : List (List (OrbView msg))
groups =
    [ [ Transmutation, Alteration, Augmentation ]
    , [ Regal, Alchemy, Chaos, Exalted ]
    , [ Blessed, Divine ]
    , [ Scouring, Annulment ]
    ]
        |> List.map (List.map orbView)


list : List (OrbView msg)
list =
    List.concat groups
