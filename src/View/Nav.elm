module View.Nav exposing (view)

import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Route exposing (Route)


view : { sim : Maybe Route, orbspam : Maybe Route } -> Html msg
view hrefs =
    -- https://getbootstrap.com/docs/4.4/components/navbar/
    div [ class "navbar navbar-expand-sm navbar-light bg-light" ]
        [ a [ class "navbar-brand", href "/" ] [ text "Crafty" ]
        , button
            [ type_ "button"
            , class "navbar-toggler"
            , attribute "data-toggle" "collapse"
            , attribute "data-target" "navbarLinks"
            ]
            [ span [ class "navbar-toggler-icon" ] [] ]
        , div [ id "navbarLinks", class "collapse navbar-collapse" ]
            [ ul [ class "navbar-nav" ]
                [ li [ class "nav-item" ] [ a [ class "nav-link", Route.href <| Maybe.withDefault (Route.Sim Nothing) hrefs.sim ] [ text "Simulator" ] ]
                , li [ class "nav-item" ] [ a [ class "nav-link", Route.href <| Maybe.withDefault (Route.OrbSpam Nothing) hrefs.orbspam ] [ text "Orb-Spam" ] ]
                , li [ class "nav-item" ] [ a [ class "nav-link", Route.href Route.Changelog ] [ text "Changelog" ] ]
                ]
            ]
        ]
