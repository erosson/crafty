module View.ItemState exposing (ItemState, create, decode, gen)

import Crafting.Item as Item exposing (Item)
import Crafting.Orb exposing (PossibleMods)
import Datamine exposing (Datamine)
import Datamine.BaseItem as BaseItem exposing (BaseItem, Influence, InfluenceConfig)
import Json.Decode as D exposing (Decoder)
import List.Extra
import Random exposing (Generator)
import Set exposing (Set)


type alias ItemState =
    { val : Item
    , possibleModsScoured : PossibleMods
    , possibleModsCurrent : PossibleMods
    }


type alias Args a =
    { a | item : String, influence : Maybe String, state : Maybe String }


decode : Datamine -> String -> Result String ItemState
decode dm =
    D.decodeString (Item.decoder dm)
        >> Result.mapError D.errorToString
        >> Result.map (create dm)


gen : Datamine -> Args a -> Maybe (Generator ItemState)
gen dm args =
    let
        influence =
            args.influence
                |> Maybe.withDefault ""
                |> BaseItem.influenceFromNames
                |> List.map .enum
    in
    dm.baseItems
        |> List.Extra.find (\i -> i.name == args.item)
        |> Maybe.map (Item.white dm influence >> Random.map (create dm))


create : Datamine -> Item -> ItemState
create dm item =
    let
        pms =
            Crafting.Orb.possibleMods dm item.influence item.ilvl item.base

        pmc =
            pms |> Crafting.Orb.filterMods item
    in
    { val = item
    , possibleModsScoured = pms
    , possibleModsCurrent = pmc
    }
