module View.Item exposing (Msg(..), view, viewDetailedMods, viewHeader, viewHeaderSelector, viewSimpleMods)

import Crafting.Item as Item exposing (Item)
import Crafting.Mod as Mod exposing (Mod)
import Crafting.Orb exposing (PossibleMods)
import Datamine exposing (Datamine)
import Datamine.BaseItem as BaseItem exposing (BaseItem)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Icon
import Session exposing (Session)
import Set exposing (Set)
import View.ItemState exposing (ItemState)


view : { a | session : Session, details : Bool, altKey : Bool, error : Maybe String, search : String } -> Item -> Html Msg
view model item =
    let
        is =
            Item.toStrings model.session.datamine item

        itemHtml =
            [ div [] <| viewHeaderSelector model.session.datamine item model.search
            , hr [] []
            ]
                ++ (if model.details /= model.altKey then
                        viewDetailedMods model.session.datamine item

                    else
                        viewSimpleMods model.session.datamine item
                   )

        errorHtml =
            case model.error of
                Nothing ->
                    []

                Just err ->
                    [ hr [] [], code [] [ text err ] ]
    in
    div [ class "col-sm item" ]
        (itemHtml ++ [ div [] errorHtml ])


viewHeader : Datamine -> Item -> List (Html msg)
viewHeader dm item =
    let
        is =
            Item.toStrings dm item
    in
    [ div [] [ img [ src is.img ] [] ]
    , div [ class "item-title", rarityClass item.magic ] [ text is.name ]
    , div [ class "item-influence" ] [ text is.influence ]
    ]


viewDetailedMods : Datamine -> Item -> List (Html msg)
viewDetailedMods dm item =
    let
        is =
            Item.toDetailedStrings dm item
    in
    [ is.implicits |> List.concatMap viewDetailedMod
    , is.affixes |> List.concatMap viewDetailedMod
    ]
        |> List.filter ((/=) [])
        |> List.map (div [])
        |> List.intersperse (hr [] [])


viewSimpleMods : Datamine -> Item -> List (Html msg)
viewSimpleMods dm item =
    let
        is =
            Item.toStrings dm item
    in
    [ is.implicits |> List.map viewStat
    , is.affixes |> List.map viewStat
    ]
        |> List.filter ((/=) [])
        |> List.map (div [])
        |> List.intersperse (hr [] [])


viewDetailedMod : Mod.DetailedStrings -> List (Html msg)
viewDetailedMod s =
    span [ class "mod-details", classList [ ( "bench-mod", Set.member "crafted" s.tags ) ] ] [ text s.label ]
        :: (s.stats |> List.map (\stat -> viewStat { tags = s.tags, stat = stat }))


viewStat : Mod.Strings -> Html msg
viewStat s =
    div [ class "mod-stat", classList [ ( "bench-mod", Set.member "crafted" s.tags ) ] ] [ text s.stat ]


rarityClass : Item.Magic -> H.Attribute msg
rarityClass m =
    class <|
        case m of
            Item.White ->
                "item-title-normal"

            Item.Blue _ _ ->
                "item-title-magic"

            Item.Yellow _ _ ->
                "item-title-rare"


type Msg
    = RequestFocus String
    | InputSearchBaseItem String
    | SelectBaseItem BaseItem
    | InputItemLevel String
    | InputInfluence BaseItem.Influence


viewHeaderSelector : Datamine -> Item -> String -> List (Html Msg)
viewHeaderSelector dm item search =
    case item.magic of
        Item.White ->
            [ div [] [ img [ src <| BaseItem.img item.base ] [] ]
            , div [] (viewBaseItemSelector dm item search)
            , div [ class "form-inline influence-selector" ]
                ([ text "Level "
                 , input
                    [ class "form-control"
                    , type_ "number"
                    , A.min "1"
                    , A.max "100"
                    , value <| String.fromInt item.ilvl
                    , onInput InputItemLevel
                    ]
                    []
                 ]
                    ++ viewInfluenceSelector item
                )
            ]

        _ ->
            viewHeader dm item


viewInfluenceSelector : Item -> List (Html Msg)
viewInfluenceSelector item =
    case Item.influenceToString item of
        Nothing ->
            []

        Just selected ->
            [ div [ class "dropdown" ]
                [ button
                    -- type=button avoids form submits
                    [ type_ "button"
                    , class "btn btn-outline-dark dropdown-toggle"
                    , A.attribute "data-toggle" "dropdown"
                    , A.attribute "data-dropup-auto" "false"
                    ]
                    [ text selected ]
                , div [ class "dropdown-menu" ]
                    (BaseItem.influences
                        |> List.map BaseItem.influenceConfig
                        |> List.map
                            (\influence ->
                                let
                                    id_ =
                                        "influence_" ++ String.toLower influence.name

                                    isChecked =
                                        List.any ((==) influence.enum) item.influence
                                in
                                button
                                    [ type_ "button"
                                    , class "dropdown-item btn btn-link"
                                    , disabled <| not isChecked && List.length item.influence >= 2
                                    , onClick <| InputInfluence influence.enum
                                    ]
                                    [ text influence.name
                                    , span [ style "float" "right" ] <|
                                        if isChecked then
                                            [ Icon.fas "check" ]

                                        else
                                            []
                                    ]
                            )
                    )
                ]
            ]


viewBaseItemSelector : Datamine -> Item -> String -> List (Html Msg)
viewBaseItemSelector dm item search =
    let
        searchId =
            "base-item-search"

        bases : List BaseItem
        bases =
            dm.baseItems
                |> List.filter (\b -> String.contains (String.toLower search) (String.toLower b.name))
                |> List.take 10
    in
    [ div [ class "dropdown" ]
        [ button
            -- type=button avoids form submits
            [ type_ "button"
            , class "btn btn-outline-dark dropdown-toggle"
            , A.attribute "data-toggle" "dropdown"
            , A.attribute "data-dropup-auto" "false"
            , onClick <| RequestFocus searchId
            ]
            -- [ img [ src <| BaseItem.img item.base ] []
            [ text item.base.name
            ]
        , div [ class "dropdown-menu" ]
            (div [ class "dropdown-item" ]
                [ input
                    [ class "form-control"
                    , id searchId
                    , value search
                    , onInput InputSearchBaseItem
                    ]
                    []
                ]
                :: (bases
                        |> List.map
                            (\base ->
                                button
                                    [ type_ "button"
                                    , class "dropdown-item btn btn-link"
                                    , onClick <| SelectBaseItem base
                                    ]
                                    [ img [ src <| BaseItem.img base ] []
                                    , text base.name
                                    ]
                            )
                   )
            )
        ]
    ]
