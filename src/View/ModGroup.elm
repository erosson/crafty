module View.ModGroup exposing (Msg(..), view)

import Crafting.Item as Item exposing (Item)
import Crafting.Orb exposing (PossibleMods)
import Datamine exposing (Datamine)
import Datamine.BaseMod as BaseMod exposing (BaseMod, SpawnWeight)
import Dict exposing (Dict)
import Dict.Extra
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import List.Extra
import Set exposing (Set)
import View.ItemState exposing (ItemState)
import View.Util exposing (percent)


{-| Mods generated the same way. Prefixes/suffixes.
-}
view : String -> Datamine -> Set String -> ItemState -> List ( Float, BaseMod ) -> List (Html Msg)
view label dm expanded item mods0 =
    let
        totalWeight =
            mods0
                |> List.filter (\( w, m ) -> Set.member m.id item.possibleModsCurrent.ids)
                |> weight
    in
    if item.val.base.domain == "flask" then
        -- flask mod groups aren't meaningful, just list all mods ungrouped
        [ div [ class "card-header" ] [ text label ]
        , ul [ class "list-group list-group-flush" ]
            (mods0
                |> List.map
                    (\( w, m ) ->
                        viewModEntry dm expanded totalWeight ( w, m )
                            |> li
                                [ class "list-group-item"
                                , classList [ ( "blocked-modgroup", not <| Set.member m.id item.possibleModsCurrent.ids ) ]
                                , listPadding
                                ]
                    )
            )
        ]

    else
        let
            unorderedGroups : Dict String (List ( Float, BaseMod ))
            unorderedGroups =
                mods0
                    |> Dict.Extra.groupBy (Tuple.second >> .group)
                    |> Dict.map (\_ -> List.sortBy (Tuple.second >> .requiredLevel))

            groups : List ( String, List ( Float, BaseMod ) )
            groups =
                mods0
                    |> List.map (Tuple.second >> .group)
                    |> List.Extra.unique
                    |> List.filterMap (\group -> Dict.get group unorderedGroups |> Maybe.map (Tuple.pair group))
        in
        [ div [ class "card-header" ] [ text label ]
        , ul [ class "list-group list-group-flush" ]
            (groups |> List.map (viewModGroup dm expanded item totalWeight))
        ]


viewModGroup : Datamine -> Set String -> ItemState -> Float -> ( String, List ( Float, BaseMod ) ) -> Html Msg
viewModGroup dm expanded item totalWeight ( group, mods ) =
    let
        weight_ =
            weight mods
    in
    case mods of
        -- if this group only has one mod, always show it directly instead of collapsing
        [ ( _, m ) as mw ] ->
            li
                [ class "list-group-item"
                , classList [ ( "blocked-modgroup", not <| Set.member m.id item.possibleModsCurrent.ids ) ]
                , listPadding
                ]
                (viewModEntry dm expanded totalWeight mw)

        [] ->
            li [] []

        (( _, m ) as mw) :: _ ->
            let
                blocked =
                    not <| Set.member m.id item.possibleModsCurrent.ids
            in
            li
                [ class "list-group-item"
                , classList [ ( "blocked-modgroup", blocked ) ]
                , listPadding
                , onClick <| ToggleGroup group
                ]
                (div []
                    ([ viewWeight (weight mods) totalWeight
                     , if Set.member group expanded then
                        span [ class "fas fa-caret-down collapse-caret" ] []

                       else
                        span [ class "fas fa-caret-right collapse-caret" ] []
                     ]
                        ++ (mods
                                |> List.map Tuple.second
                                |> BaseMod.listToStrings dm
                                |> List.map (\s -> div [ title <| "mod group: " ++ group ] [ text s ])
                           )
                     -- )
                    )
                    :: (if Set.member group expanded then
                            [ ul [ class "list-group" ]
                                (mods |> List.map (viewModEntry dm expanded totalWeight >> li [ class "list-group-item", listPadding ]))
                            ]

                        else
                            []
                       )
                )


listPadding =
    class "py-1"


weight : List ( Float, BaseMod ) -> Float
weight =
    List.map Tuple.first >> List.sum


viewModEntry : Datamine -> Set String -> Float -> ( Float, BaseMod ) -> List (Html Msg)
viewModEntry dm expanded totalWeight ( weight_, mod ) =
    [ div [ onClick <| SelectMod mod ]
        (span [ class "required-level" ] [ text <| String.fromInt mod.requiredLevel ]
            :: viewWeight weight_ totalWeight
            :: (BaseMod.toStrings dm mod
                    |> List.map (\s -> div [ title mod.id ] (s |> String.lines |> List.map (\l -> div [] [ text l ])))
               )
        )
    ]


viewWeight : Float -> Float -> Html msg
viewWeight w total =
    if total <= 0 then
        div [] []

    else
        div
            [ class "chance"
            , title <| "weight: " ++ String.fromFloat w ++ "/" ++ String.fromFloat total
            ]
            [ text <| percent <| w / total ]


type Msg
    = ToggleGroup String
    | SelectMod BaseMod
