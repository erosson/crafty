module View.Util exposing (..)


percent : Float -> String
percent p =
    (p * 100 |> String.fromFloat |> String.left 5) ++ "%"


nbsp : String
nbsp =
    "\u{00A0}"


formatFloat : Int -> Float -> String
formatFloat digits f =
    let
        s =
            String.fromFloat f
    in
    case String.split "." s of
        [ head, tail ] ->
            -- try to trim sigfigs a bit
            if String.length head >= digits then
                head

            else
                String.left (digits + 1) s

        [ _ ] ->
            -- no decimal, no problem
            s

        _ ->
            -- TODO french decimals? 1.234.567,89 vs 1,234,567.89
            s
