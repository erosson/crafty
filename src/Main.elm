module Main exposing (main)

import Browser
import Browser.Navigation as Nav
import Html exposing (Html)
import Html.Attributes as A
import Page.Changelog
import Page.DebugBaseItems
import Page.DebugMod
import Page.DebugStat
import Page.DebugStatEntry
import Page.DebugTable
import Page.OrbSpam
import Page.Sim
import Route exposing (Route)
import Session exposing (Flags, Session)
import Url exposing (Url)


type alias Model =
    Result Error Page


type alias Error =
    String


type Page
    = NotFound Session
    | Sim Page.Sim.Model
    | OrbSpam Page.OrbSpam.Model
    | Changelog Session
    | DebugStat Session
    | DebugStatEntry Session
    | DebugMod Session
    | DebugBaseItems Session
    | DebugTable Page.DebugTable.Model


type Msg
    = OnUrlChange Url
    | OnUrlRequest Browser.UrlRequest
    | SimMsg Page.Sim.Msg
    | OrbSpamMsg Page.OrbSpam.Msg


init : Flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url nav =
    case Session.init flags nav of
        Err err ->
            ( Err err, Cmd.none )

        Ok session ->
            routeTo (Route.parse url) session
                |> Tuple.mapFirst Ok


view : Model -> Browser.Document Msg
view model =
    { title = "Crafty"
    , body =
        abandoned
            :: (case model of
                    Err err ->
                        [ Html.pre [] [ Html.text err ] ]

                    Ok ok ->
                        case ok of
                            NotFound session ->
                                notFound

                            OrbSpam m ->
                                Page.OrbSpam.view m
                                    |> List.map (Html.map OrbSpamMsg)

                            Sim m ->
                                Page.Sim.view m
                                    |> List.map (Html.map SimMsg)

                            Changelog session ->
                                Page.Changelog.view session

                            DebugStat session ->
                                Page.DebugStat.view session

                            DebugStatEntry session ->
                                Page.DebugStatEntry.view session

                            DebugMod session ->
                                Page.DebugMod.view session

                            DebugBaseItems session ->
                                Page.DebugBaseItems.view session

                            DebugTable m ->
                                Page.DebugTable.view m
                                    |> Maybe.withDefault notFound
               )
    }


abandoned : Html msg
abandoned =
    Html.div [ A.class "container alert alert-warning" ] [ Html.a [ A.title "they finished first", A.href "https://www.craftofexile.com/" ] [ Html.h1 [] [ Html.text "Use craftofexile.com instead" ], Html.text "This tool is abandoned" ] ]


notFound =
    [ Html.pre [] [ Html.text "404 not found" ] ]


toSession : Page -> Session
toSession page =
    case page of
        NotFound s ->
            s

        OrbSpam m ->
            m.session

        Sim m ->
            m.session

        Changelog s ->
            s

        DebugStat s ->
            s

        DebugStatEntry s ->
            s

        DebugMod s ->
            s

        DebugBaseItems s ->
            s

        DebugTable m ->
            m.session


routeTo : Maybe Route -> Session -> ( Page, Cmd Msg )
routeTo mroute session =
    case mroute of
        Nothing ->
            ( NotFound session, Cmd.none )

        Just r ->
            case r of
                Route.OrbSpam plan ->
                    Page.OrbSpam.init session plan
                        |> Tuple.mapBoth OrbSpam (Cmd.map OrbSpamMsg)

                Route.Sim args ->
                    Page.Sim.init session args
                        |> Tuple.mapBoth Sim (Cmd.map SimMsg)

                Route.Changelog ->
                    ( Changelog session, Cmd.none )

                Route.DebugStat ->
                    ( DebugStat session, Cmd.none )

                Route.DebugStatEntry ->
                    ( DebugStatEntry session, Cmd.none )

                Route.DebugMod ->
                    ( DebugMod session, Cmd.none )

                Route.DebugBaseItems ->
                    ( DebugBaseItems session, Cmd.none )

                Route.DebugTable t ->
                    ( DebugTable <| Page.DebugTable.init session t, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Err err ->
            ( model, Cmd.none )

        Ok okModel ->
            case ( msg, okModel ) of
                ( OnUrlChange url, _ ) ->
                    routeTo (Route.parse url) (toSession okModel)
                        |> Tuple.mapFirst Ok

                ( OnUrlRequest (Browser.Internal url), _ ) ->
                    ( model, url |> Url.toString |> Nav.pushUrl (toSession okModel).nav )

                ( OnUrlRequest (Browser.External url), _ ) ->
                    ( model, Nav.load url )

                ( OrbSpamMsg msg_, OrbSpam model_ ) ->
                    Page.OrbSpam.update msg_ model_
                        |> Tuple.mapBoth (Ok << OrbSpam) (Cmd.map OrbSpamMsg)

                ( OrbSpamMsg msg_, _ ) ->
                    ( model, Cmd.none )

                ( SimMsg msg_, Sim model_ ) ->
                    Page.Sim.update msg_ model_
                        |> Tuple.mapBoth (Ok << Sim) (Cmd.map SimMsg)

                ( SimMsg msg_, _ ) ->
                    ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        Ok (Sim m) ->
            Page.Sim.subscriptions m
                |> Sub.map SimMsg

        _ ->
            Sub.none


main : Program Flags Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = OnUrlChange
        , onUrlRequest = OnUrlRequest
        }
