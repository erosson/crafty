port module Ports exposing (..)

{-| -}


{-| Alt seems to be a browser shortcut that removes focus?

End result: [alt down > alt up > alt down] fails. Ports let us use preventDefault().

-}
port modDetailsKey : ({ isDown : Bool } -> msg) -> Sub msg
