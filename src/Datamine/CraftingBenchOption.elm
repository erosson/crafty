module Datamine.CraftingBenchOption exposing
    ( CraftingBenchOption
    , decoder
    , mod
    )

import Datamine.BaseMod as BaseMod exposing (BaseMod)
import Dict exposing (Dict)
import Json.Decode as D exposing (Decoder)
import Json.Decode.Pipeline as P


type alias CraftingBenchOption =
    { benchGroup : String
    , benchTier : Int
    , cost : List ( String, Int )
    , itemClasses : List String
    , master : String
    , modId : String
    }


type alias Datamine a =
    { a
        | modsById : Dict String BaseMod
    }


mod : Datamine a -> CraftingBenchOption -> Maybe BaseMod
mod dm bench =
    Dict.get bench.modId dm.modsById


decoder : D.Decoder CraftingBenchOption
decoder =
    D.succeed CraftingBenchOption
        |> P.required "bench_group" D.string
        |> P.required "bench_tier" D.int
        |> P.required "cost" (D.keyValuePairs D.int)
        |> P.required "item_classes" (D.list D.string)
        |> P.required "master" D.string
        |> P.required "mod_id" D.string
