module Datamine.BaseMod exposing
    ( BaseMod
    , ModStat
    , SpawnWeight
    , decoder
    , listToStats
    , listToStrings
    , toStats
    , toStrings
    )

{-| <https://github.com/brather1ng/RePoE/blob/master/RePoE/docs/mods.md>
<https://raw.githubusercontent.com/brather1ng/RePoE/master/RePoE/data/mods.json>
<https://www.reddit.com/r/pathofexiledev/comments/52ubsn/how_to_link_item_mods_to_their_stat_descriptions>
-}

import Datamine.Stat as Stat exposing (Stat)
import Dict exposing (Dict)
import Json.Decode as D exposing (Decoder)


type alias SpawnWeight =
    { tag : String, weight : Int }


type alias BaseMod =
    { name : String
    , group : String
    , type_ : String
    , domain : String
    , generationType : String
    , stats : List ModStat
    , spawnWeights : List SpawnWeight
    , requiredLevel : Int
    , addsTags : List String

    -- this goes last, for ease of decoding
    , id : String
    }


type alias ModStat =
    { id : String
    , min : Int
    , max : Int
    }


type alias Datamine a =
    { a
        | statsById : Dict String Stat
    }


toStats : Datamine a -> BaseMod -> List Stat
toStats dm =
    .stats
        >> List.map .id
        >> Stat.fromIds dm.statsById


listToStats : Datamine a -> List BaseMod -> List Stat
listToStats dm =
    List.concatMap .stats
        >> List.map .id
        >> Stat.fromIds dm.statsById


statRanges : Datamine a -> List BaseMod -> Dict String ( Int, Int )
statRanges dm =
    let
        fold : ModStat -> Dict String ( Int, Int ) -> Dict String ( Int, Int )
        fold stat =
            Dict.update stat.id
                (Maybe.withDefault ( stat.min, stat.max )
                    >> (\( fmin, fmax ) -> ( min stat.min fmin, max stat.max fmax ))
                    >> Just
                )
    in
    List.concatMap .stats
        >> List.foldl fold Dict.empty


toStrings : Datamine a -> BaseMod -> List String
toStrings dm mod =
    listToStrings dm [ mod ]


listToStrings : Datamine a -> List BaseMod -> List String
listToStrings dm mods =
    let
        vals : Dict String ( Int, Int )
        vals =
            statRanges dm mods
    in
    listToStats dm mods
        |> List.map (\s -> Stat.toStringRange s vals)
        |> List.filterMap
            (\r ->
                case r of
                    Err err ->
                        Nothing

                    Ok "" ->
                        Nothing

                    Ok ok ->
                        Just ok
            )


decoder : Decoder (String -> BaseMod)
decoder =
    D.map7 BaseMod
        (D.field "name" D.string)
        (D.field "group" D.string)
        (D.field "type" D.string)
        (D.field "domain" D.string)
        (D.field "generation_type" D.string)
        (D.map3 ModStat
            (D.field "id" D.string)
            (D.field "min" D.int)
            (D.field "max" D.int)
            |> D.list
            |> D.field "stats"
        )
        (D.field "spawn_weights"
            (D.map2 SpawnWeight
                (D.field "tag" D.string)
                (D.field "weight" D.int)
                |> D.list
            )
        )
        |> D.andThen
            (\mod ->
                D.map2 mod
                    (D.field "required_level" D.int)
                    (D.field "adds_tags" <| D.list D.string)
            )
