module Datamine.Stat exposing
    ( Condition
    , Stat
    , StatLang
    , StatLangEntry
    , ValHandler
    , byId
    , decoder
    , fromIds
    , numValues
    , toStringDefault
    , toStringRange
    , toStringVal
    )

{-| <https://github.com/brather1ng/RePoE/blob/master/RePoE/docs/stat_translations.md>
<https://raw.githubusercontent.com/brather1ng/RePoE/master/RePoE/data/stat_translations.json>
-}

import Dict exposing (Dict)
import Dict.Extra
import Json.Decode as D exposing (Decoder)
import List.Extra
import Maybe.Extra
import View.Util as Util


type alias Stat =
    { id : String
    , ids : List String
    , en : StatLang

    -- , lang : List StatLang
    }


type alias StatLang =
    List StatLangEntry


type alias StatLangEntry =
    { string : String
    , vals : List ValHandler
    }


type alias ValHandler =
    { condition : Condition
    , format : Formatter
    , handler : List Handler
    }


type alias Condition =
    { min : Maybe Int
    , max : Maybe Int
    }


type Formatter
    = Ignored
    | Val
    | Plus
    | Percent
    | PlusPercent


type Handler
    = Div Float
    | Mul Float
    | Add Float
    | Negate
    | Identity


numValues : Stat -> Int
numValues =
    .en
        >> List.concatMap .vals
        >> List.map .format
        >> List.Extra.count ((/=) Ignored)


condition : Condition -> Int -> Bool
condition c val =
    (c.min |> Maybe.Extra.unwrap True (\m -> val >= m))
        && (c.max |> Maybe.Extra.unwrap True (\m -> val <= m))


conditions : StatLangEntry -> List Int -> Bool
conditions e vals =
    List.all (\( vh, val ) -> condition vh.condition val)
        (List.map2 Tuple.pair e.vals vals)


toEntry : Stat -> List Int -> Maybe StatLangEntry
toEntry stat vlist =
    List.Extra.find (\e -> conditions e vlist) stat.en


toStringDefault : Stat -> Maybe String
toStringDefault =
    .en >> List.head >> Maybe.map .string


toStringVal : Stat -> Dict String Int -> Result String String
toStringVal stat vals =
    let
        vlist : List Int
        vlist =
            stat.ids
                |> List.map
                    (\id ->
                        vals
                            |> Dict.get id
                            |> Maybe.withDefault 0
                    )
    in
    case toEntry stat vlist of
        Nothing ->
            Err <| "no translations: " ++ stat.id

        Just entry ->
            vlist
                |> List.map2 Tuple.pair entry.vals
                |> List.map (\( vh, v ) -> ( v, renderVal vh v ))
                |> render entry


toStringRange : Stat -> Dict String ( Int, Int ) -> Result String String
toStringRange stat vals =
    let
        vlist : List ( Int, Int )
        vlist =
            stat.ids
                |> List.map
                    (\id ->
                        vals
                            |> Dict.get id
                            |> Maybe.withDefault ( 0, 0 )
                    )
    in
    case toEntry stat (List.map Tuple.second vlist) of
        Nothing ->
            Err <| "no translations: " ++ stat.id

        Just entry ->
            vlist
                |> List.map2 Tuple.pair entry.vals
                |> List.map
                    (\( vh, ( min, max ) ) ->
                        ( max
                        , if min == max then
                            renderVal vh max

                          else
                            "(" ++ renderVal vh min ++ " to " ++ renderVal vh max ++ ")"
                        )
                    )
                |> render entry


renderVal : ValHandler -> Int -> String
renderVal vh v =
    List.foldl applyHandler (toFloat v) vh.handler |> applyFormatter vh.format


applyFormatter : Formatter -> Float -> String
applyFormatter f v0 =
    let
        v =
            Util.formatFloat 4 v0
    in
    case f of
        Val ->
            v

        Plus ->
            (if v0 > 0 then
                "+"

             else
                ""
            )
                ++ v

        Percent ->
            v ++ "%"

        PlusPercent ->
            (if v0 > 0 then
                "+"

             else
                ""
            )
                ++ v
                ++ "%"

        Ignored ->
            -- TODO error?
            ""


applyHandler : Handler -> Float -> Float
applyHandler h v =
    case h of
        Identity ->
            v

        Negate ->
            -v

        Add n ->
            v + n

        Mul n ->
            v * n

        Div n ->
            v / n



--let fold : String -> Int -> Int
--    format : String -> Int -> String
--    format =
--in List.foldl fold v vh.handler |> format vh.format


render : StatLangEntry -> List ( Int, String ) -> Result String String
render e vals =
    if List.length vals /= List.length e.vals then
        Err <| "stat needs " ++ String.fromInt (List.length e.vals) ++ " args, but got " ++ String.fromInt (List.length vals)

    else if not <| conditions e <| List.map Tuple.first vals then
        Err <| "stat conditions not met"

    else
        let
            replacers : List Replacer
            replacers =
                List.map2 Replacer e.vals (List.map Tuple.second vals)
                    |> List.indexedMap (\i v -> v i)
        in
        List.foldl replace e.string replacers |> Ok


type alias Replacer =
    { vh : ValHandler, val : String, index : Int }


replace : Replacer -> String -> String
replace r =
    let
        target =
            "{" ++ String.fromInt r.index ++ "}"
    in
    String.replace target r.val


{-| Index stats by id.

Stats have multiple ids - one per associated number. I'd like to know if more
than one stat uses the same id, so Err during parsing if that happens.
(That's why we're not using Dict.Extra.fromListBy here.)

-}
byId : List Stat -> Result String (Dict String Stat)
byId stats =
    let
        statsListById : Dict String (List Stat)
        statsListById =
            stats
                |> List.concatMap (\s -> s.ids |> List.map (\id -> ( id, s )))
                |> Dict.Extra.groupBy Tuple.first
                |> Dict.map (always <| List.map Tuple.second)

        nonuniques =
            statsListById |> Dict.filter (\k vs -> List.length vs /= 1)
    in
    if Dict.size nonuniques > 0 then
        Err <| "nonunique stat ids: " ++ String.join ", " (Dict.keys nonuniques)

    else
        statsListById
            |> Dict.toList
            |> List.filterMap (\( k, vs ) -> List.head vs |> Maybe.map (Tuple.pair k))
            |> Dict.fromList
            |> Ok


fromIds : Dict String Stat -> List String -> List Stat
fromIds stats =
    List.filterMap (\id -> Dict.get id stats) >> List.Extra.uniqueBy .id


decoder : Decoder Stat
decoder =
    D.map3 Stat
        (D.field "ids" <| D.index 0 D.string)
        (D.field "ids" <| D.list D.string)
        (D.field "English" statLangDecoder)


statLangDecoder : Decoder StatLang
statLangDecoder =
    D.list statLangEntryDecoder


statLangEntryDecoder : Decoder StatLangEntry
statLangEntryDecoder =
    D.map2 StatLangEntry
        (D.field "string" D.string)
        (D.map3 (List.map3 ValHandler)
            (D.field "condition" <| D.list conditionDecoder)
            (D.field "format" <| D.list formatterDecoder)
            (D.field "index_handlers" <| D.list <| D.list handlerDecoder)
        )


handlerDecoder : Decoder Handler
handlerDecoder =
    D.string
        |> D.andThen
            (\s ->
                -- https://github.com/OmegaK2/PyPoE/blob/dev/PyPoE/poe/file/translations.py#L1883
                case s of
                    "divide_by_one_hundred" ->
                        D.succeed <| Div 100

                    "divide_by_one_hundred_2dp" ->
                        D.succeed <| Div 100

                    "times_twenty" ->
                        D.succeed <| Mul 20

                    "negate" ->
                        D.succeed Negate

                    "per_minute_to_per_second" ->
                        D.succeed <| Div 60

                    "per_minute_to_per_second_0dp" ->
                        D.succeed <| Div 60

                    "per_minute_to_per_second_1dp" ->
                        D.succeed <| Div 60

                    "per_minute_to_per_second_2dp" ->
                        D.succeed <| Div 60

                    "per_minute_to_per_second_2dp_if_required" ->
                        D.succeed <| Div 60

                    "milliseconds_to_seconds" ->
                        D.succeed <| Div 1000

                    "milliseconds_to_seconds_0dp" ->
                        -- TODO rounding?
                        D.succeed <| Div 1000

                    "milliseconds_to_seconds_2dp" ->
                        -- TODO rounding?
                        D.succeed <| Div 1000

                    "milliseconds_to_seconds_2dp_if_required" ->
                        -- TODO rounding?
                        D.succeed <| Div 1000

                    "deciseconds_to_seconds" ->
                        D.succeed <| Div 10

                    "old_leech_percent" ->
                        D.succeed <| Div 5

                    "old_leech_permyriad" ->
                        D.succeed <| Div 500

                    "divide_by_twelve" ->
                        D.succeed <| Div 12

                    "divide_by_six" ->
                        D.succeed <| Div 6

                    "divide_by_ten_0dp" ->
                        D.succeed <| Div 10

                    "divide_by_two_0dp" ->
                        D.succeed <| Div 2

                    "divide_by_fifteen_0dp" ->
                        D.succeed <| Div 15

                    "divide_by_twenty_then_double_0dp" ->
                        -- TODO
                        D.succeed <| Div 10

                    "30%_of_value" ->
                        D.succeed <| Mul 0.3

                    "60%_of_value" ->
                        D.succeed <| Mul 0.6

                    "multiplicative_damage_modifier" ->
                        D.succeed <| Add 100

                    "canonical_stat" ->
                        D.succeed Identity

                    "mod_value_to_item_class" ->
                        D.succeed Identity

                    _ ->
                        D.fail <| "unrecognized stat handler: " ++ s
            )


formatterDecoder : Decoder Formatter
formatterDecoder =
    D.string
        |> D.andThen
            (\s ->
                case s of
                    "#" ->
                        D.succeed Val

                    "+#" ->
                        D.succeed Plus

                    "#%" ->
                        D.succeed Percent

                    "+#%" ->
                        D.succeed PlusPercent

                    "ignore" ->
                        D.succeed Ignored

                    _ ->
                        D.fail <| "unrecognized stat formatter: " ++ s
            )


conditionDecoder : Decoder Condition
conditionDecoder =
    D.map2 Condition
        (D.maybe <| D.field "min" D.int)
        (D.maybe <| D.field "max" D.int)
