module Datamine.BaseItem exposing
    ( BaseItem
    , Influence(..)
    , InfluenceConfig
    , benchMods
    , decoder
    , img
    , influenceConfig
    , influenceFromName
    , influenceFromNames
    , influenceTags
    , influenceToName
    , influenceToNames
    , influenceToggle
    , influences
    , possibleMods
    )

import Datamine.BaseMod as BaseMod exposing (BaseMod, SpawnWeight)
import Datamine.CraftingBenchOption as CraftingBenchOption exposing (CraftingBenchOption)
import Dict exposing (Dict)
import Dict.Extra
import Json.Decode as D exposing (Decoder)
import List.Extra
import Set exposing (Set)


type alias BaseItem =
    { name : String
    , domain : String
    , tags : Set String
    , implicits : List String
    , itemClass : String
    , visualIdentity : BaseItemVisualIdentity
    , releaseState : String
    , id : String
    }


type alias BaseItemVisualIdentity =
    { ddsFile : String, id : String }


type alias Datamine a =
    { a
        | modsByDomain : Dict String (List BaseMod)
        , modsById : Dict String BaseMod
        , craftingBenchOptionsByItemClass : Dict String (List CraftingBenchOption)
    }


type Influence
    = Shaper
    | Elder
      -- Baran
    | Crusader
      -- Veritania
    | Redeemer
      -- Al-Hezmin
    | Hunter
      -- Drox
    | Warlord


influences =
    [ Shaper, Elder, Crusader, Redeemer, Hunter, Warlord ]


type alias InfluenceConfig =
    { enum : Influence
    , name : String
    , tagSuffix : String
    }


influenceConfig : Influence -> InfluenceConfig
influenceConfig influence =
    case influence of
        Shaper ->
            InfluenceConfig influence "Shaper" "shaper"

        Elder ->
            InfluenceConfig influence "Elder" "elder"

        Crusader ->
            InfluenceConfig influence "Crusader" "crusader"

        Redeemer ->
            InfluenceConfig influence "Redeemer" "eyrie"

        Hunter ->
            InfluenceConfig influence "Hunter" "basilisk"

        Warlord ->
            InfluenceConfig influence "Warlord" "adjudicator"


influenceByName : Dict String InfluenceConfig
influenceByName =
    influences
        |> List.map influenceConfig
        |> Dict.Extra.fromListBy (.name >> String.toLower)


influenceFromName : String -> Maybe InfluenceConfig
influenceFromName name =
    Dict.get (String.toLower name) influenceByName


influenceFromNames : String -> List InfluenceConfig
influenceFromNames =
    String.split ","
        >> List.filterMap influenceFromName
        >> List.take 2


influenceToName : Influence -> String
influenceToName =
    influenceConfig >> .name


influenceToggle : Influence -> List Influence -> List Influence
influenceToggle inf infs =
    -- not the most efficient algorithm, but we never have more than 2 influences anyway
    let
        removed =
            List.filter (\i -> i /= inf) infs
    in
    if List.length infs == List.length removed then
        inf :: infs

    else
        removed


influenceToNames : List Influence -> Maybe String
influenceToNames infs =
    if infs == [] then
        Nothing

    else
        infs
            |> List.Extra.uniqueBy influenceToName
            |> List.take 2
            |> List.map influenceToName
            |> String.join ","
            |> Just


{-| Item classes with possible influenced mods.

Influence is expressed as tags, and named like <itemclass>\_<influencer>.

-}
itemClassToInfluenceTagPrefix : Dict String String
itemClassToInfluenceTagPrefix =
    Dict.fromList
        [ ( "Amulet", "amulet" )
        , ( "Body Armour", "body_armour" )
        , ( "Boots", "boots" )
        , ( "Gloves", "gloves" )
        , ( "Helmet", "helmet" )
        , ( "Shield", "shield" )
        , ( "Belt", "belt" )
        , ( "Quiver", "quiver" )
        , ( "Ring", "ring" )
        , ( "Claw", "claw" )
        , ( "Dagger", "dagger" )
        , ( "Rune Dagger", "dagger" )
        , ( "One Hand Axe", "axe" )
        , ( "One Hand Mace", "mace" )
        , ( "Sceptre", "sceptre" )
        , ( "One Hand Sword", "sword" )
        , ( "Thrusting One Hand Sword", "sword" )
        , ( "Wand", "wand" )
        , ( "Bow", "bow" )
        , ( "Staff", "staff" )
        , ( "Warstaff", "staff" )
        , ( "Two Hand Axe", "2h_axe" )
        , ( "Two Hand Mace", "2h_mace" )
        , ( "Two Hand Sword", "2h_sword" )
        ]


influenceTags : BaseItem -> Maybe (List ( InfluenceConfig, String ))
influenceTags item =
    Dict.get item.itemClass itemClassToInfluenceTagPrefix
        |> Maybe.map
            (\tagPrefix ->
                influences
                    |> List.filterMap
                        (\i ->
                            let
                                c =
                                    influenceConfig i
                            in
                            influenceTag item c |> Maybe.map (Tuple.pair c)
                        )
            )


influenceTag : BaseItem -> InfluenceConfig -> Maybe String
influenceTag item c =
    Dict.get item.itemClass itemClassToInfluenceTagPrefix
        |> Maybe.map (\tagPrefix -> tagPrefix ++ "_" ++ c.tagSuffix)


possibleMods : Datamine a -> List InfluenceConfig -> Int -> BaseItem -> List ( BaseMod, SpawnWeight )
possibleMods dm influence ilvl item =
    -- TODO: delve, incursion, essence
    let
        implicits : List BaseMod
        implicits =
            item.implicits |> List.filterMap (\id -> Dict.get id dm.modsById)

        tags : Set String
        tags =
            (influence |> List.filterMap (influenceTag item))
                ++ (implicits |> List.concatMap .addsTags)
                |> Set.fromList
                |> Set.union item.tags

        mods =
            Dict.get item.domain dm.modsByDomain |> Maybe.withDefault []
    in
    (mods
        |> List.filter (\m -> m.requiredLevel <= ilvl)
        |> List.filterMap (\m -> spawnWeight tags m |> Maybe.map (Tuple.pair m))
    )
        |> List.filter (\( m, w ) -> w.weight > 0)


benchMods : Datamine a -> BaseItem -> List ( CraftingBenchOption, BaseMod )
benchMods dm item =
    Dict.get item.itemClass dm.craftingBenchOptionsByItemClass
        |> Maybe.withDefault []
        |> List.filterMap (\c -> CraftingBenchOption.mod dm c |> Maybe.map (Tuple.pair c))


spawnWeight : Set String -> BaseMod -> Maybe SpawnWeight
spawnWeight tags mod =
    List.Extra.find (\w -> Set.member w.tag tags) mod.spawnWeights


img : BaseItem -> String
img item =
    let
        png : String
        png =
            Maybe.withDefault ("image/" ++ String.replace ".dds" ".png" item.visualIdentity.ddsFile) (imgOverrides item)
    in
    "https://web.poecdn.com/" ++ png ++ "?scale=1"


decoder : Decoder (Maybe (String -> BaseItem))
decoder =
    D.field "item_class" D.string
        |> D.andThen
            (\cls ->
                if Set.member cls baseItemClasses then
                    D.map7 BaseItem
                        (D.field "name" D.string)
                        (D.field "domain" D.string)
                        (D.field "tags" <| D.map Set.fromList <| D.list D.string)
                        (D.field "implicits" <| D.list D.string)
                        (D.field "item_class" D.string)
                        (D.field "visual_identity" <|
                            D.map2 BaseItemVisualIdentity
                                (D.field "dds_file" D.string)
                                (D.field "id" D.string)
                        )
                        (D.field "release_state" D.string)
                        |> D.map Just

                else
                    D.succeed Nothing
            )


baseItemClasses : Set String
baseItemClasses =
    Set.fromList
        [ "Amulet"
        , "Body Armour"
        , "Boots"
        , "Gloves"
        , "Helmet"
        , "Shield"

        -- , "StackableCurrency"
        , "Belt"

        -- , "Currency"
        -- , "DivinationCard"
        , "HybridFlask"
        , "LifeFlask"
        , "ManaFlask"
        , "UtilityFlaskCritical"
        , "UtilityFlask"

        -- , "Active Skill Gem"
        -- , "Support Skill Gem"
        , "AbyssJewel"
        , "Jewel"
        , "Quiver"
        , "Ring"
        , "Claw"
        , "Dagger"
        , "Rune Dagger"
        , "One Hand Axe"
        , "One Hand Mace"
        , "Sceptre"
        , "One Hand Sword"
        , "Thrusting One Hand Sword"
        , "Wand"
        , "FishingRod"
        , "Bow"
        , "Staff"
        , "Warstaff"
        , "Two Hand Axe"
        , "Two Hand Mace"
        , "Two Hand Sword"
        ]


imgOverrides : BaseItem -> Maybe String
imgOverrides { id } =
    -- Flasks images are different. They're generated from a png with 3 parts: (front, back, juice).
    -- Example: https://web.poecdn.com/image/Art/2DItems/Flasks/lifeflask11.png?scale=1
    -- Generated flask urls look like "gen/image/<base64-flask-url-and-fill-percent>/<??checksum??>/Item.png"
    -- I can't figure out how to generate new urls. The checksum might be a secure hash designed to prevent this.
    -- Instead, I've copied urls from my stash on www.pathofexile.com for each flask.
    --
    -- TODO this is very incomplete. Fill in other flasks! can get them from trade search, but those images are empty
    case id of
        -- "Metadata/Items/Flasks/FlaskLife10" ->
        -- sanctified
        -- Just "gen/image/WzksNCx7ImYiOiJBcnRcLzJESXRlbXNcL0ZsYXNrc1wvbGlmZWZsYXNrMTAiLCJzcCI6MC42MDg1LCJsZXZlbCI6MH1d/284f73b2c1/Item.png"
        "Metadata/Items/Flasks/FlaskLife11" ->
            -- divine
            Just "gen/image/WzksNCx7ImYiOiJBcnRcLzJESXRlbXNcL0ZsYXNrc1wvbGlmZWZsYXNrMTEiLCJzcCI6MC42MDg1LCJsZXZlbCI6MX1d/75809f4d7a/Item.png"

        "Metadata/Items/Flasks/FlaskUtility5" ->
            -- granite flask
            Just "gen/image/WzksNCx7ImYiOiJBcnRcLzJESXRlbXNcL0ZsYXNrc1wvZ3Jhbml0ZSIsInNwIjowLjYwODUsImxldmVsIjoxfV0/1c0ea1042d/Item.png"

        "Metadata/Items/Flasks/FlaskUtility10" ->
            -- basalt flask (phys reduction)
            Just "gen/image/WzksNCx7ImYiOiJBcnRcLzJESXRlbXNcL0ZsYXNrc1wvc3RvbmUiLCJzcCI6MC42MDg1LCJsZXZlbCI6MX1d/a48ebdcded/Item.png"

        "Metadata/Items/Flasks/FlaskUtility14" ->
            -- silver flask (onslaught)
            Just "gen/image/WzksNCx7ImYiOiJBcnRcLzJESXRlbXNcL0ZsYXNrc1wvc2lsdmVyIiwic3AiOjAuNjA4NSwibGV2ZWwiOjF9XQ/b4edcba80f/Item.png"

        "Metadata/Items/Flasks/FlaskUtility6" ->
            Just "gen/image/WzksNCx7ImYiOiJBcnRcLzJESXRlbXNcL0ZsYXNrc1wvc3ByaW50Iiwic3AiOjAuNjA4NSwibGV2ZWwiOjF9XQ/20490e4a76/Item.png"

        _ ->
            Nothing
