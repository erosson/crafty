module Datamine exposing
    ( Datamine
    , decoder
    )

import Datamine.BaseItem as BaseItem exposing (BaseItem)
import Datamine.BaseMod as BaseMod exposing (BaseMod, ModStat)
import Datamine.CraftingBenchOption as CraftingBenchOption exposing (CraftingBenchOption)
import Datamine.Stat as Stat exposing (Stat, StatLang, StatLangEntry)
import Dict exposing (Dict)
import Dict.Extra
import Json.Decode as D exposing (Decoder)
import Maybe.Extra
import Result.Extra


type alias RawDatamine =
    { statTranslations : List Stat
    , mods : List BaseMod
    , baseItems : List BaseItem
    , craftingBenchOptions : List CraftingBenchOption
    }


type alias Datamine =
    { statTranslations : List Stat
    , mods : List BaseMod
    , baseItems : List BaseItem
    , craftingBenchOptions : List CraftingBenchOption

    --
    , statsById : Dict String Stat
    , modsById : Dict String BaseMod
    , modsByDomain : Dict String (List BaseMod)
    , baseItemsById : Dict String BaseItem
    , craftingBenchOptionsByItemClass : Dict String (List CraftingBenchOption)
    }


init : RawDatamine -> Result String Datamine
init raw =
    Result.map
        (\statsById ->
            { statTranslations = raw.statTranslations
            , mods = raw.mods
            , baseItems = raw.baseItems
            , craftingBenchOptions = raw.craftingBenchOptions
            , statsById = statsById

            -- indexes
            , modsById = raw.mods |> Dict.Extra.fromListBy .id
            , modsByDomain = raw.mods |> Dict.Extra.groupBy .domain
            , baseItemsById = raw.baseItems |> Dict.Extra.fromListBy .id
            , craftingBenchOptionsByItemClass =
                raw.craftingBenchOptions
                    |> List.concatMap (\c -> c.itemClasses |> List.map (Tuple.pair c))
                    |> Dict.Extra.groupBy Tuple.second
                    |> Dict.map (\_ -> List.map Tuple.first)
            }
        )
        (Stat.byId raw.statTranslations)


decoder : Decoder Datamine
decoder =
    D.map4 RawDatamine
        (D.field "stat_translations" <| D.list Stat.decoder)
        (D.field "mods" <| D.map (List.map (\( k, v ) -> v k)) <| D.keyValuePairs BaseMod.decoder)
        (D.field "base_items" <| D.map (List.filterMap (\( k, mv ) -> mv |> Maybe.map (\v -> v k))) <| D.keyValuePairs BaseItem.decoder)
        (D.field "crafting_bench_options" <| D.list CraftingBenchOption.decoder)
        |> D.map init
        |> D.andThen decodeResult


decodeResult : Result String v -> D.Decoder v
decodeResult r =
    case r of
        Err err ->
            D.fail err

        Ok v ->
            D.succeed v
