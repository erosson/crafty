module Plan exposing (Plan, decode, empty, encode, fromItem, mdecode)

import Base64
import Crafting.Item exposing (Item)
import Crafting.Orb exposing (Orb)
import Datamine exposing (Datamine)
import Dict exposing (Dict)
import Json.Decode as D
import Json.Encode as E
import View.Criteria as Criteria exposing (CriteriaEntry)
import View.ItemState exposing (ItemState)


type alias Plan =
    { item : ItemState
    , recipe : List Orb
    , criteria : List CriteriaEntry
    , iterations : Int
    }


empty : Datamine -> Result String Plan
empty dm =
    Crafting.Item.default dm
        |> Result.map (fromItem dm)


fromItem : Datamine -> Item -> Plan
fromItem dm item =
    { item = View.ItemState.create dm item
    , recipe = []
    , criteria = []
    , iterations = 10000
    }


encode : Plan -> String
encode =
    encoder
        >> E.encode 0
        >> Base64.encode


decode : Datamine -> String -> Result String Plan
decode dm =
    Base64.decode
        >> Result.andThen (D.decodeString (decoder dm) >> Result.mapError D.errorToString)


mdecode : Datamine -> Maybe String -> Result String Plan
mdecode dm mstr =
    case mstr of
        Nothing ->
            empty dm

        Just str ->
            decode dm str


encoder : Plan -> E.Value
encoder plan =
    E.object
        [ ( "item", plan.item.val |> Crafting.Item.encoder )
        , ( "recipe", plan.recipe |> E.list Crafting.Orb.encoder )
        , ( "criteria", plan.criteria |> E.list Criteria.encoder )
        , ( "iterations", plan.iterations |> E.int )
        ]


decoder : Datamine -> D.Decoder Plan
decoder dm =
    D.map4 Plan
        (D.field "item" <| D.map (View.ItemState.create dm) <| Crafting.Item.decoder dm)
        (D.field "recipe" <| D.list Crafting.Orb.decoder)
        (D.field "criteria" <| D.list (Criteria.decoder dm))
        (D.field "iterations" D.int)
