const fs = require('fs').promises
const stat_translations = require('../third-party/RePoE/RePoE/data/stat_translations.json')
const mods = require('../third-party/RePoE/RePoE/data/mods.json')
const base_items = require('../third-party/RePoE/RePoE/data/base_items.json')
const crafting_bench_options = require('../third-party/RePoE/RePoE/data/crafting_bench_options.json')

async function main() {
  const datamine = {base_items, mods, stat_translations, crafting_bench_options}
  const tmpl = await fs.readFile('./tests/Fixture/Json.elm.template', 'utf8')
  // Stringifying twice escapes the JSON string so it pastes into Elm cleanly.
  // Elm isn't JSON, but the escaping seems similar enough!
  const elm = tmpl.replace('%(datamine)s', JSON.stringify(JSON.stringify(datamine)))
  return await fs.writeFile('./tests/Fixture/Json.elm', elm)
}
main()
